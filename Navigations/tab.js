import React from 'react';
import { SafeAreaView } from 'react-native';
//Componets
import AutosScreen from '../Screens/Autos';
import PromosScreen from '../Screens/Promotions';
import PagosScreen from '../Screens/Pagos';
import CotizarScreen from '../Screens/Contizar';
import CitasScreen from '../Screens/Citas';
import { BottomNavigation } from 'react-native-paper';

function TabNavigation (props){
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([

        { key: 'home', title: 'AUTOS', icon: require('../assets/icons/volante.png'), color: '#f26522'},
        { key: 'citas', title: 'CITAS', icon: require('../assets/icons/calendario.png'), color: '#009688' },
        { key: 'cotizar', title: 'COTIZAR', icon: require('../assets/icons/cotizacion.png'), color: '#1f419b'  },
        { key: 'pagos', title: 'PAGOS', icon: require('../assets/icons/pago.png'), color: '#2E9AFE' },
        { key: 'recents', title: 'PROMOS', icon: require('../assets/icons/rebaja.png'), color: '#6d6e71' },
    ]);

    const autos = () => <AutosScreen {...props} />;
    const citas = () =>  <CitasScreen {...props}/>;
    const cotizar = () => <CotizarScreen {...props}/>;
    const pagos = () => <PagosScreen {...props}/>;
    const promotions = () => <PromosScreen {...props}/>;
  
    const renderScene = BottomNavigation.SceneMap({
      home: autos,
      citas: citas ,
      cotizar: cotizar ,
      pagos: pagos ,
      recents: promotions,
    });

    return (
        <BottomNavigation
        navigationState={{ index, routes }}
        onIndexChange={setIndex}
        renderScene={renderScene}
        barStyle={{ color: "#fff"}}
      />

    );
  };

export default TabNavigation;