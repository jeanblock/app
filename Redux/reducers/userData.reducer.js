import { DATAUSER } from '../types';

const  INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case DATAUSER :
            return  action.payload;
        default: return state
    }
}
