import {GETVEHICLE} from '../types';

const INITIAL_STATE = [];

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case GETVEHICLE:
            return  [
                ...INITIAL_STATE,
                ...action.payload
            ];
        default: return state
    }
};