import {AUTORIZATION} from '../types';

const INITIAL_STATE = false;

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case AUTORIZATION :
            return  action.payload;
        default: return state
    }
};