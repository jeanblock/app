import { GETVEHICLE } from '../types';
import AsyncStorage from '@react-native-community/async-storage';
//import AsyncStorage from '@react-native-async-storage/async-storage';
import { API } from '../../apis/const'

export const getVehicleActions = () => async (dispatch) => {

    dispatch({
        type:GETVEHICLE,
        payload: []
    });

    const token   = await AsyncStorage.getItem('@tokens');
    const idSales = await AsyncStorage.getItem('@idSales');

    console.log("token -> ", token);
    console.log("idSalesforce -> ", idSales);

    await fetch(`${API}/api/v1/users/assets/${idSales}`, {
        method: 'GET',
        headers : {
            'Content-Type': 'application/json',
            'Authorization': token,
        }
    })
    .then( data => data.json() )
    .then( (data)=> {
        if(data.code == 201){
            dispatch({
                type:GETVEHICLE,
                payload: data.message
            });
        }
    })
    .catch( err => {
        console.log(err)
    } )
};

export const noProductActions = () => async (dispatch) => {
    dispatch({
        type: GETVEHICLE ,
        payload: []
    })
};