import { SAVEPHOTO } from '../../types';

export const savePhotoAction = (photo, photoNew) => (dispatch) => {
    return dispatch({
        type: SAVEPHOTO,
        payload: {photo, photoNew}
    });
};