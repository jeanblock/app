import { CARDSTORE } from '../types';
//import AsyncStorage from '@react-native-community/async-storage';

export const addAndDeleteProduct = ( product ) => async (dispatch) => {

    function removeDuplicates(originalArray, prop) {
        var newArray = [];
        var lookupObject  = {};

        for(var i in originalArray) {
           lookupObject[originalArray[i][prop]] = originalArray[i];
        }
        for(i in lookupObject) {
            newArray.push(lookupObject[i]);
        }
         return newArray;
    }
    const productList = removeDuplicates(product, "id")

    dispatch({
        type: CARDSTORE ,
        payload: productList
    })
};