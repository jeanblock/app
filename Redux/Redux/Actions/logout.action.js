import {GETVEHICLE, SAVEPHOTO, DATAUSER, GETPRODUCT, CARDSTORE} from '../../types';
import AsyncStorage from '@react-native-community/async-storage';
//import AsyncStorage from '@react-native-async-storage/async-storage';

export const logoutData = () => async (dispatch) => {

    dispatch({
        type: GETVEHICLE ,
        payload: []
    })
    dispatch({
        type: SAVEPHOTO ,
        payload: {}
    })

    dispatch({
        type: DATAUSER,
        payload: {}
    })

    dispatch({
        type: GETPRODUCT,
        payload: []
    })

    dispatch({
        type: CARDSTORE,
        payload: []
    })
    await AsyncStorage.removeItem('@userId');
    await AsyncStorage.removeItem('@tokens');
    await AsyncStorage.removeItem('@idSales');
}