import { LOGIN } from '../../types';
import AsyncStorage from '@react-native-community/async-storage';
//import AsyncStorage from '@react-native-async-storage/async-storage';

export const loginActions = (id, data) => (dispatch) => {
    async function storeData(value){
        try {
          await AsyncStorage.setItem('@tokens', value);
          await AsyncStorage.setItem('@userId', id.toString());
        } catch (e) {
          console.log( "ERROR => " + e)
        }
    };
    storeData(data);

    return dispatch({
        type: LOGIN,
        payload: id
    });
};