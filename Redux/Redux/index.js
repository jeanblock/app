import { combineReducers } from 'redux';
import autorization from './reducers/autorizarionReducer';
import savePhoto from './reducers/savePhoto';
import loginReducer from './reducers/login.reducer';
import redirectReducer from './reducers/redirect.reducer';
import userData from './reducers/userData.reducer';
import producData from './reducers/product.Reducer';
import vehicleData from './reducers/vehicleData';
import cardStore from './reducers/cardStore.reducer';
export default combineReducers({
    autorization,
    savePhoto,
    loginReducer,
    redirectReducer,
    userData,
    producData,
    vehicleData,
    cardStore
});