export const AUTORIZATION = 'AUTORIZARITZAION';
export const SAVEPHOTO = "savePhoto";
export const LOGIN = "login";
export const REDIRECT = "REDIRECT";
export const DATAUSER = "userDataInfo";
export const GETPRODUCT = "getproductos";
export const GETVEHICLE = "getvehicle";
export const CARDSTORE = "carritodeCompras";