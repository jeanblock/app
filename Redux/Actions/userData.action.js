import { API } from '../../apis/const'

import {DATAUSER} from '../types';
import AsyncStorage from '@react-native-community/async-storage';
//import AsyncStorage from '@react-native-async-storage/async-storage';

export const dataUserActions = (redirection) => async (dispatch) => {

    const token = await AsyncStorage.getItem('@tokens');
    const id = await AsyncStorage.getItem('@userId');

    await fetch(`${API}/api/v1/users/${id}`, {
        method: 'GET',
        headers : {
            'Content-Type': 'application/json',
            'Authorization': token,
        }
    }).then( data => data.json() )
    .then( async (data)=> {
        await AsyncStorage.setItem('@idSales', data.message.user_sf_id)
        if(redirection){
            redirection();
        }
        dispatch({
            type: DATAUSER,
            payload: data.message
        })
    })
    .catch()
};

export const dataUserLogoutActions = () => async (dispatch) => {

    dispatch({
        type: DATAUSER,
        payload: {}
    })

};