import { GETPRODUCT} from '../types';

export const getProductActions = (data) => async (dispatch) => {

    dispatch({
        type: GETPRODUCT ,
        payload: data
    })
  
};