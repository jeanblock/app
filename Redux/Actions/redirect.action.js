import { REDIRECT } from '../types';

export const redictActions = (data) => (dispatch) => {
    return dispatch({
        type: REDIRECT,
        payload: data
    });
};