let colorBar = "#FF0040";

export const titles = {fontSize: 22, fontWeight: '700'};
export const subtitle = { fontSize: 15, color: "#848484", padding: 10 };
export const primary = colorBar;
export const red = "#eb0a1e";
export const theme = {
    colors: {
        placeholder: '#A4A4A4',
        background: '#0d1828',
        text: '#585858',
        primary: '#eb0a1e',
        blue: "#1B357E"
    }
};
export const themeTransparent = {
    colors: {
        placeholder: '#A4A4A4',
        background: '#ffffff00',
        text: '#585858',
        primary: '#eb0a1e'
    }
};