import {  API } from './const';
export async function loginAPI(info){

    const data = await fetch(`${API}/login`, {
        method: 'POST',
        headers:{
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(info)
    })
    .then(data => data.json())
    .then(data => {
        console.log(data);
        return data
    })
    .catch(err => err);
    return data;
}