import {  API } from './const';
export async function registerAPI(info){

    const data = await fetch(`${API}/api/v1/users`, {
        method: 'POST',
        headers:{
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(info)
    }).then(data => data.json())
    .then(data => data)
    .catch(err => err);
    return data;
}