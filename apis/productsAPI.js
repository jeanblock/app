import { API } from './const';
import AsyncStorage from '@react-native-community/async-storage';
//import AsyncStorage from '@react-native-async-storage/async-storage';

export const getProducts = async () => {
    const token = await AsyncStorage.getItem('@tokens');

    return new Promise( (resolve, reject) => {
        fetch(`${API}/api/v1/products`, {
            method: 'GET',
            headers : {
                'Content-Type': 'application/json',
                'Authorization': token,
            }
        })
        .then( data => data.json() )
        .then( (data)=> resolve(data.message))
        .catch( err => reject(err));
    } )
}
export const getAllCategory = async () => {
    const token = await AsyncStorage.getItem('@tokens');

    return new Promise( (resolve, reject) => {
        fetch(`${API}/api/v1/products/onlycategory/`, {
            method: 'GET',
            headers : {
                'Authorization': token,
            }
        })
        .then(data =>  data.json())
        .then( (data)=> resolve(data.message))
        .catch( err => reject(err));
    })
}
export const getProductsCategory = async (category) => {
    const token = await AsyncStorage.getItem('@tokens');

    return new Promise( (resolve, reject) => {
        fetch(`${API}/api/v1/products/category/${category}`, {
            method: 'GET',
            headers : {
                'Content-Type': 'application/json',
                'Authorization': token,
            }
        })
        .then( data => data.json() )
        .then( data => resolve(data.message))
        .catch( err => reject(err));
    } )
}
export const getSubcategoryInCategory = async (category) => {
    const token = await AsyncStorage.getItem('@tokens');
    return new Promise( (resolve, reject) => {
        fetch(`${API}/api/v1/products/onlysubcategory/${category}`, {
            method: 'GET',
            headers : {
                'Content-Type': 'application/json',
                'Authorization': token,
            }
        })
        .then( data => data.json() )
        .then( data =>  resolve(data.message))
        .catch( err => reject(err));
    } )
}
export const getProductsSubcategory = async (category, subCategory) => {
    const token = await AsyncStorage.getItem('@tokens');

    return new Promise( (resolve, reject) => {
        fetch(`${API}/api/v1/products/category/${category}/subcategory/${subCategory}`, {
            method: 'GET',
            headers : {
                'Content-Type': 'application/json',
                'Authorization': token,
            }
        })
        .then(data => data.json())
        .then( (data)=> resolve(data.message))
        .catch( err => reject(err));
    } )
}
export const getProductsAplications = async (category, subCategory) => {
    const token = await AsyncStorage.getItem('@tokens');

    return new Promise( (resolve, reject) => {
        fetch(`${API}/api/v1/products/onlyapplication/${category}/subcategory/${subCategory}`, {
            method: 'GET',
            headers : {
                'Content-Type': 'application/json',
                'Authorization': token,
            }
        })
        .then(data => data.json())
        .then( (data)=> resolve(data.message))
        .catch( err => reject(err));
    } )
}
export const getProductsApplication = async (category, subCategory, application) => {
    const token = await AsyncStorage.getItem('@tokens');

    return new Promise( (resolve, reject) => {
        fetch(`${API}/api/v1/products/category/${category}/subcategory/${subCategory}/application/${application}`, {
            method: 'GET',
            headers : {
                'Content-Type': 'application/json',
                'Authorization': token,
            }
        })
        .then( data => data.json() )
        .then( (data)=> resolve(data.message))
        .catch( err => reject(err));
    } )
}