import {  API } from './const';
import AsyncStorage from '@react-native-community/async-storage';
//import AsyncStorage from '@react-native-async-storage/async-storage';

export async function getAmount(id, numbers){
    const token = await AsyncStorage.getItem('@tokens');
    return new Promise( (resolve, reject) => {
        fetch(`${API}/api/v1/payment/getAmount`,{
            method: 'POST',
            headers : {
                'Content-Type': 'application/json',
                'Authorization': token,
            },
            body: JSON.stringify({
                "codigo_cliente": id,
                "numero_prestamo": numbers
            })
        })
        .then( data =>  data.json())
        .then( data => resolve(data))
        .catch(e => reject(e))
    })
}