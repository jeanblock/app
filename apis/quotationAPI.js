import { API } from './const';
import AsyncStorage from '@react-native-community/async-storage';

export const generarCotizacion = async (data) => {
    const token = await AsyncStorage.getItem('@tokens');

    return new Promise( (resolve, reject) => {
        fetch(`${API}/api/v1/quotes`, {
            method: 'POST',
            headers : {
                'Content-Type': 'application/json',
                'Authorization': token,
            },
            body:  JSON.stringify(data)
        })
        .then( data => data.json() )
        .then( (data)=> resolve(data))
        .catch( err => reject(err));
    } )
}