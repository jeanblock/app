import React, { useState , useEffect } from 'react';
import { View , Text, SafeAreaView, ScrollView, Image, StyleSheet, Modal} from 'react-native';
import {Button} from 'react-native-paper';
import ProfileComponents from '../../components/profile';
import {titles, subtitle, theme , themeTransparent} from '../../theme/vars';
import CardModePay from './CardModePay';
import TarjetaData from './TarjetaData';
import {getAmount} from '../../apis/payments';
import { connect } from 'react-redux';

const PagosScreen = (props) => {
    const [heigthOne, setHeigthOne] = useState(200);
    const [heigthTwo, setHeigthTwo] = useState(80);
    const [heigthThree, setHeigtThree] = useState(80);
    const [loading, setLoading] = useState(false);
    const [numbers, setNumbers] = useState("10004059");

    function hableCuota(){
        setLoading(true)

        getAmount("1000034531", "10004059").then(e => {
            console.log(e)
            setHeigthOne(80);
            setHeigthTwo(200);
            console.log(e.message[e.message.length -1].SALDO_LPS);
            setNumbers( e.message[e.message.length -1].SALDO_LPS)
            setLoading(false)

        }).catch(e => {
            setLoading(false)
            console.log(e)
        })
    }
    function habldeOpen(){
        setHeigthOne(200)
    }

    function hablenext() {
        setHeigthOne(80);
        setHeigthTwo(200);
    }
    function hablenexTwo() {
        setHeigthTwo(80);
        setHeigtThree(400)
    }
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ScrollView centerContent={true}>
                <View style={{ flex: 1 }} >
                    <View style={{ height: 120, borderBottomLeftRadius: 20, borderBottomRightRadius: 20,
                        overflow: 'hidden' , backgroundColor: "#fff", marginBottom: 20, paddingTop: 8}}>
                        <ProfileComponents navigate={props.navigation} />
                    </View>
                    <View  style={{ flex: 0.8, padding: 16, backgroundColor: "#fff"}}>
                        <Text style={titles}>
                                Pago de Cuotas de Vehículo
                        </Text>
                        <Text style={{fontSize: 15, color: "#2a2a2a", padding: 0, marginTop: 10}}>
                            Pago válido únicamente con su tarjeta de crédito BAC Credomatic - Grupo Flores.
                        </Text>
                        <Image
                            source={ require('../../assets/images/payment-cards-bac.png') }
                            style={{ width: "100%", margin: 0, resizeMode: 'contain', margin: 0, marginTop: 0, padding: 0, height: 80 }}
                        />
                        <View>
                            <CardModePay
                                icon="../../assets/icons/paper.png"
                                title="Número de prestamo"
                                action={hableCuota}
                                height={heigthOne}
                                siguiente={habldeOpen}
                                next={hablenext}
                                type="one"
                                values={numbers}
                            />
                            <CardModePay
                                title="Monto a pagar"
                                icon="../../assets/icons/money.png"
                                action={hablenexTwo}
                                height={heigthTwo}
                                siguiente={null}
                                next={hablenexTwo}
                                type="two"
                                values={numbers}
                            />
                            <TarjetaData
                                title="Datos de la tarjeta"
                                icon="../../assets/icons/money.png"
                                action={hableCuota}
                                height={heigthThree}
                                siguiente={null}
                                type="three"
                                values={numbers}
                            />
                        </View>
                    </View>
                    <Button mode="contained"
                        color="#EB0A1E"
                        style={{ marginHorizontal: 10, marginTop: 20 }}
                        >Confirmar Pago
                    </Button>
                    <Text style={{fontSize: 15, color: "#848484", padding: 15 }}>
                        No almacenamos la información de pago, únicamente la utilizamos para procesar cada transacción. Consulte los términos y condiciones aquí.
                    </Text>
                    <Modal visible={loading} transparent={true}>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: "#24242480" }}>
                            <View style={{ backgroundColor: "#fff", width: '80%', justifyContent: 'center', alignItems: 'center', padding: 16, borderRadius: 10 }}>
                                <Text style={{ fontSize: 20,  }}>Cargando...</Text>
                            </View>
                        </View>
                    </Modal>
                </View>
            </ScrollView>
        </SafeAreaView>

    );
  };

const Style = StyleSheet.create({
    paymentInfoBox :{
        flexDirection: 'row', justifyContent: 'flex-start',
        alignItems: 'center', paddingHorizontal: 15,
        paddingVertical: 15, borderRadius: 10
    },
    iconConten: {
        backgroundColor: '#2e9afe', padding: 15, borderRadius: 10, marginRight: 30
    },
    contenItem:{ justifyContent: 'space-between', height: 80, width: '70%' },
    itemTitle: { fontSize: 14,
        color: "#fff", margin: 0, fontWeight: '800'
    },
    itemSubtitle: {
        fontSize: 16,
        color: "#fff", margin: 0, fontWeight: '800'
    },
    icon:{
        width: 30, height: 30
    }
})
const mapStateToProps = (state) => state;
export default connect(mapStateToProps, {})(PagosScreen);