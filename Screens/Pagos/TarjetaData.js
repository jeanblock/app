import React, { useState} from 'react';
import {StyleSheet , View} from 'react-native';
import {TextInput, Button, Avatar, Card,  IconButton } from 'react-native-paper';
import {themeTransparent, } from '../../theme/vars';



export default function TarjetaData(props) {
    const { siguiente } = props;

    const [ccv, setCcV] = useState("");
    const [name, setName] = useState("");
    const [numberTar, setNumberTar] = useState();
    const [venceTar, setVenceTar] = useState();

    const hableVence = (e) => {
        if(e.length <= 5){
            if(e.length == 2){
                let palabra = e + "/";
                console.log(palabra)
                setVenceTar(palabra)
            }else{
                setVenceTar(e)
            }
        }
    }
    const hableCode = (e) => {
        if(e.length <= 3){
            setCcV(e)
        }
    }
    return(
        <Card style={{ 
            backgroundColor: '#0084ff14', 
            marginVertical: 10, 
            borderRadius: 16, borderWidth: 1, borderColor: '#2E9AFE',
            height: props.height,
            overflow: 'hidden',
        }}>
            <Card.Title
                title={props.title}
               
                right={(props) => <IconButton {...props} icon={require('../../assets/icons/openIcon.png')}  
                    color="#2E9AFE"
                    size={18}
                    style={{ position: 'relative', right: 10 }}
                    onPress={siguiente}
                    />
                }
                titleStyle={{  color: "#000", fontSize: 20, fontWeight: '400' }}
            />
            <Card.Content>
                <TextInput
                    value={name}
                    style={{ margin: 0,  padding: 0, alignSelf: "stretch", height: 50, borderRadius: 0, marginVertical: 16 }}
                    onChangeText={ e => setName(e)}
                    theme={themeTransparent}
                    placeholder="Nombre del tarjetahabiente"
                />
                <TextInput
                    value={numberTar}
                    style={{ margin: 0,  padding: 0, alignSelf: "stretch", height: 50, borderRadius: 0, marginVertical: 16 }}
                    onChangeText={ e => setNumberTar(e)}
                    theme={themeTransparent}
                    placeholder="Número de tarjeta"
                    keyboardType="numeric"
                />
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 16 }}>
                    <TextInput
                        value={venceTar}
                        style={{ margin: 0,  padding: 0, alignSelf: "stretch", height: 50, width: "45%", 
                            borderRadius: 0 }}
                        onChangeText={ e => hableVence(e)}
                        theme={themeTransparent}
                        placeholder="Ejem: 07/23"
                        label="Fecha de vencimiento"
                        keyboardType="numeric"
                    />
                    <TextInput
                        value={ccv}
                        style={{ margin: 0,  padding: 0, alignSelf: "stretch", height: 45, width: "50%",
                            borderRadius: 0 }}
                        onChangeText={ e => hableCode(e)}
                        theme={themeTransparent}
                        placeholder="Código de seguridad"
                        keyboardType="numeric"
                    />
                </View>
               
            </Card.Content>
           
            <Card.Actions style={{ justifyContent: 'space-around', padding: 16 }}>
                <Button mode="text"
                    color="#2E9AFE"
                    style={{ width: 120, borderRadius: 10 }}
                   
                >Cancel</Button>
                <Button mode="contained"
                    color="#2E9AFE"
                    style={{ width: 120, borderRadius: 10 }}
                    onPress={props.next}
                >Siguiente</Button>
            </Card.Actions>
        </Card>
    )
}
const Style = StyleSheet.create({
    paymentInfoBox :{
        flexDirection: 'row', justifyContent: 'flex-start',
        alignItems: 'center', paddingHorizontal: 15,
        paddingVertical: 15, borderRadius: 10
    },
    iconConten: {
        backgroundColor: '#2e9afe', padding: 15, borderRadius: 10, marginRight: 30
    },
    contenItem:{ justifyContent: 'space-between', height: 80, width: '70%' },
    itemTitle: { fontSize: 14,
        color: "#fff", margin: 0, fontWeight: '800'
    },
    itemSubtitle: {
        fontSize: 16,
        color: "#fff", margin: 0, fontWeight: '800'
    },
    icon:{
        width: 30, height: 30
    }
})