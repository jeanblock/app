import React, {useState} from 'react';
import {StyleSheet, TextInput, Text } from 'react-native';
import {Button, Avatar, Card,  IconButton } from 'react-native-paper';
import {themeTransparent, } from '../../theme/vars';



export default function CardModePay(props) {

    const { siguiente } = props;
   

    function hableActions(){
        props.action()
    }
    return(
        <Card style={{ 
            backgroundColor: '#0084ff14', 
            marginVertical: 10, 
            borderRadius: 16, borderWidth: 1, borderColor: '#2E9AFE',
            height: props.height,
            overflow: 'hidden',
        }}>
                <Card.Title
                    title={props.title}
                    right={(props) => <IconButton {...props} icon={require('../../assets/icons/openIcon.png')}  
                    color="#2E9AFE"
                    size={18}
                    style={{ position: 'relative', right: 10 }}
                   
                />
                }
                titleStyle={{  color: "#000", fontSize: 20, fontWeight: '400' }}
            />
            <Card.Content>
                <TextInput
                    value={props.value}
                    style={{ margin: 0,  padding: 0, alignSelf: "stretch", height: 50, borderRadius: 0 }}
                    value={props.values}
                    theme={themeTransparent}
                    style={{ textAlign: 'center', fontSize: 25, fontWeight: '600', color: "#4d4d4d", borderBottomColor: "#2E9AFE", borderBottomWidth: 1,
                        paddingBottom: 8, marginBottom: 10
                    }}
                />
            </Card.Content>
           
            <Card.Actions style={{ justifyContent: 'space-around', padding: 16 }}>
                { props.type != "one" ? <Button mode="text"
                    color="#2E9AFE"
                    style={{ width: 120, borderRadius: 10 }}
                   
                >Cancelar</Button>
                : null }
                
                <Button mode="contained"
                    color="#2E9AFE"
                    style={{ width: 150, borderRadius: 10 }}
                    onPress={hableActions}
                >
                    <Text style={{ color: "#fff" }}>
                        Siguiente
                    </Text>
                </Button>
            </Card.Actions>
        </Card>
    )
}
const Style = StyleSheet.create({
    paymentInfoBox :{
        flexDirection: 'row', justifyContent: 'flex-start',
        alignItems: 'center', paddingHorizontal: 15,
        paddingVertical: 15, borderRadius: 10
    },
    iconConten: {
        backgroundColor: '#2e9afe', padding: 15, borderRadius: 10, marginRight: 30
    },
    contenItem:{ justifyContent: 'space-between', height: 80, width: '70%' },
    itemTitle: { fontSize: 14,
        color: "#fff", margin: 0, fontWeight: '800'
    },
    itemSubtitle: {
        fontSize: 16,
        color: "#fff", margin: 0, fontWeight: '800'
    },
    icon:{
        width: 30, height: 30
    }
})