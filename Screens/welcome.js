import React, {useCallback} from 'react';
import { View, Text , Image,  ImageBackground, Linking} from 'react-native';
import { Button} from 'react-native-paper';

export default function WelcomeScree(props){
    const supportedURL = "https://demo.appgrupoflores.com/terminos";
    const supportedURLTwo = "https://demo.appgrupoflores.com/privacidad";

    const handlePress = useCallback(async () => {
        await Linking.openURL(supportedURL);
      }, []);
    const handlePressTwo = useCallback(async () => {
        await Linking.openURL(supportedURLTwo);
    }, []);
    return(
        <ImageBackground
            source={require('../assets/images/bgView.jpg')}
            style={{
                flex: 1,
                resizeMode: "cover",
                justifyContent: "center",
                paddingVertical: 20
            }}
        >

            <View style={{ flex: 1}}>
                <View style={{ flex: 2, padding: 16 , paddingTop: 40}}>
                    <Image
                        source={ require('../assets/images/logos/Logo-GF-blanco.png') }
                        style={{ width: '100%', height: 150 , resizeMode: 'contain', marginTop: 50}}
                    />
                </View >
                <View style={{ flex: 0.8, padding: 16 }}>
                    <Button mode="contained"
                        color="#fff"
                        style={{ marginVertical: 10, borderRadius: 20 }}
                        onPress={ ()=> props.navigation.navigate("Login") }
                    >

                        <Text style={{ textTransform: 'none', color: "#FF0040" }} >
                        Iniciar sesión
                        </Text>
                    </Button>
                    <Button mode="contained"
                        color="#fff"
                        style={{ marginVertical: 10, borderRadius: 20 }}
                        onPress={ ()=> props.navigation.navigate("Register") }
                    >
                        <Text style={{ textTransform: 'none', color: "#FF0040" }} >
                            Crear una cuenta
                        </Text>

                    </Button>
                    <Text style={{ color: "#fff" }}>
                        Al registrarse, iniciar sesión y hacer uso de nuestra app, usted acepta nuestros
                        <Text style={{ fontWeight: '700' }} onPress={handlePress}> términos y condiciones</Text>,
                        también acepta nuestra <Text style={{ fontWeight: '700' }} onPress={handlePressTwo}>política de privacidad de datos</Text>.
                    </Text>

                </View>

            </View>
        </ImageBackground>
    )
}