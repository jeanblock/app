import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, FlatList } from 'react-native';
import { Avatar , Button , TextInput, Title, Subheading, List } from 'react-native-paper';
import InfoUserView from './InfoUser';
import { connect } from 'react-redux';
import {dataUserActions} from '../../Redux/Actions/userData.action';
import {logoutData} from '../../Redux/Actions/logout.action';

function Profile(props){

    async function logout(){
       await props.logoutData();
       await props.navigation.navigate("welcome");
    };
    function update(){
        props.dataUserActions()
    }
    useEffect( () => {
        update()
    }, [] );

    return(
        <SafeAreaView>
            <ScrollView>
                <View style={{ alignItems: 'center', padding: 16, backgroundColor: '#f26522',
                }}>
                    <Avatar.Image size={100} source={{ uri: props.userData.photo_url }} />
                    <Text style={{ color: "#fff", fontSize: 24, fontWeight: '700', marginBottom: 16, marginTop: 20 }}>
                        { props.userData.first_name + " " + props.userData.last_name }
                    </Text>
                </View>
                <View style={{ backgroundColor: "#fff", height: '100%' }}>
                    <InfoUserView
                        name={ props.userData.first_name + " " + props.userData.last_name }
                        id={props.userData.identification_number}
                        email={props.userData.email}
                        phone={props.userData.phone}
                        civilStatus={props.userData.civil_status}
                        ocupation={props.userData.ocupation}
                        genre={props.userData.genre}
                        birthdate={props.userData.birthdate}
                    />
                    <Button
                        mode="outlined"
                        style={{margin: 20, }}
                        onPress={logout}
                        color="#eb0a1e"
                    >
                        Finalizar sesión
                    </Button>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
};
const mapStateToProps = (state) => state;
const mapDispachToProps = {
    dataUserActions,
    logoutData
}
export default connect(mapStateToProps, mapDispachToProps)(Profile);