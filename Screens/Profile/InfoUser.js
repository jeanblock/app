import React from 'react';
import { List , Divider} from 'react-native-paper';
import {titles} from '../../theme/vars';

export default function InfoUserView(props){
    return (
        <List.Section>
            <List.Subheader style={titles}>Mi información personal:</List.Subheader>
                <List.Item
                    title={props.name}
                    description="Nombre"
                    //right={props => <List.Icon {...props} icon="folder" /> }
                />
            <Divider />
            <List.Item
                title={props.id}
                description="Número de identificación"
            />
            <Divider />
            <List.Item
                title={props.email}
                description="Correo electrónico"
            />
            <Divider />
            <List.Item
                title={props.phone}
                description="Teléfono"
            />
            <Divider />
            <List.Item
                title={props.civilStatus}
                description="Estado civil"
            />
            <Divider />
            <List.Item
                title={props.ocupation}
                description="Ocupación"
            />
            <Divider />
            <List.Item
                title={props.genre}
                description="Género"
            />
            <Divider />
            <List.Item
                title={props.birthdate}
                description="Fecha de nacimiento"
            />
        </List.Section>
    )
}