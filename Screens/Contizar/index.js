import React, { useState, useEffect } from 'react';
import { View , Text,  SafeAreaView, FlatList, StyleSheet, Modal, TouchableOpacity } from 'react-native';
import { Searchbar, ActivityIndicator } from 'react-native-paper';
import {titles, red} from '../../theme/vars';
import Filters from './filters';
import SubCategoriesFilter from './subCategoriesFilter';
import AplicationFilter from './aplicationFilter';

import ItemProduct from '../../components/Products/item';
import StoreCar from '../../components/StoreCar';
import { connect } from 'react-redux';
import {getProductActions} from '../../Redux/Actions/GetProduct.action'
import CardScreen from '../../components/StoreCar/storeScreen';
import {addAndDeleteProduct} from '../../Redux/Actions/cardStore.action';

import {getProducts, getAllCategory} from '../../apis/productsAPI';


const CotizarScreen = (props) => {
    const [searchQuery, setSearchQuery] = useState('');
    const [modalV, setModalV] = useState(false);
    const [filts, setFilst] = useState([]);
    const [selecItem, setSelecItem] = useState("");
    const [listSubCategory, setSubcategory] = useState([]);
    const [listApplication, setListApplication] = useState([]);

    const [categorySelect, setCategorySelect] = useState("");
    const [subCategorySelect, setSubCategorySelect] = useState("");
    const [applicationSelect, setApplicationSelect] = useState("");
    const [loading, setLoading] = useState(true);

    function selecteElemets(e){
       setSelecItem(e);
       let arrayCard = props.cardStore;
       arrayCard.push(e)
       props.addAndDeleteProduct(arrayCard);
    };

    const onChangeSearch = (query)=> {
        setSearchQuery(query);
    };

    const showModal = () => setModalV(true);

    function update(data){
        setLoading(true);
        props.getProductActions(data);
        setLoading(false);
    };

    useEffect( () => {
        getProducts().then(data => {
            update(data)
        });
        getAllCategory().then(data => {
            setFilst(data);

        }).catch(e => {
            console.log(e)
        })
    }, [] );

    const hableGetSubcategory = (data, category) => {
        setSubcategory(data);
        setCategorySelect(category);
    };
    const handleGetSubcategoryRender = (data, subCategory) => {
        setSubCategorySelect(subCategory);
        props.getProductActions(data);
    };

    const hableGetApplication = (data) => {
        setListApplication(data);
    };
    const hableRenderApplicacion = (data, application) => {
        props.getProductActions(data);
        setApplicationSelect(application);
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={{ height: '100%' }}>
                <View style={{ flex: 1, backgroundColor: "#fff", paddingBottom: 0 }} >
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={[titles, { marginHorizontal: 16, marginTop: 16 }]} >
                            Cotiza tus productos
                        </Text>
                        <View style={{ top: 8, right: 8 }} >
                            <StoreCar show={showModal}/>
                        </View>
                    </View>
                    <View style={{ marginTop: 0, flex: 1 }}>
                        <Searchbar
                            placeholder="Buscar por nombre o tipo"
                            onChangeText={onChangeSearch}
                            value={searchQuery}
                            style={Styles.search}
                            icon={require('../../assets/icons/search.png')}
                            clearIcon={ require('../../assets/icons/cancel.png') }
                        />
                        <View style={{ flex: 0.08 , padding: 8 , flexDirection: 'row'}} >

                            <Filters
                                info={filts}
                                action={update}
                                getSub={hableGetSubcategory}
                            />

                            {
                               listSubCategory.length >= 1 ?
                                <SubCategoriesFilter
                                    category={categorySelect}
                                    info={listSubCategory}
                                    action={handleGetSubcategoryRender}
                                    getApplication={hableGetApplication}
                                />
                               : null
                            }
                            {
                                subCategorySelect != "" ?
                                    <AplicationFilter
                                        category={categorySelect}
                                        subCategory={subCategorySelect}
                                        info={listApplication}
                                        action={hableRenderApplicacion}
                                    />
                                : null
                            }
                            <View></View>
                        </View>
                        <View style={Styles.contenList}>

                        {loading == true ?
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                                <ActivityIndicator animating={true} color={red}/>
                            </View>
                            :
                            <FlatList
                                data={props.producData}
                                keyExtractor={(item) => item.id.toString()}
                                showsVerticalScrollIndicator={false}
                                renderItem={({item}) => {
                                    let select = props.cardStore.filter( i => i.id == item.id);
                                    let isSelect = select.length >= 1 ? true : false ;

                                    return <ItemProduct
                                            data={item}
                                            id={item.id}
                                            name={item.name}
                                            price={item.price_isv}
                                            description={item.application}
                                            brand={item.brand}
                                            img={item.picture_url}
                                            event={selecteElemets}
                                            selecItem={isSelect}
                                            typeCover={item.tyre_cover ? item.tyre_cover : ""}
                                            lubType={item.lub_type ? item.lub_type : ""}
                                            presentation={item.presentation ? item.presentation : ""}
                                            viscosity={item.viscosity ? item.viscosity : ""}
                                        />

                                 }}
                            />

                        }
                        </View>
                    </View>
                </View>
            </View>

           { /*
            <ModalCotizacion visible={modalV}
                hiden={hidenModal}
            />
           */}
            <Modal visible={modalV} transparent={true}  animationType="slide" >
                <View style={{ flex:1 , backgroundColor: '#0000004f' }} >
                    <TouchableOpacity style={{ flex: 1 }}  onPress={ ()=> setModalV(false) }  >
                    </TouchableOpacity>
                    <View style={{ flex:2 }} >
                        <CardScreen />
                    </View>
               </View>
            </Modal>
        </SafeAreaView>
    );
};

const Styles = StyleSheet.create({
    search: {margin: 16},
    contenList: {
        backgroundColor: '#F2F2F2',
        padding: 10,
        flex:  1, marginTop: 16, borderTopRightRadius: 16, borderTopLeftRadius: 16,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,

    },
    btn:{ marginTop: 16, borderRadius: 20 }
});
const mapStateToProps = (state) => state;
const mapDispachToProps = {
    getProductActions,
    addAndDeleteProduct
}
export default connect(mapStateToProps, mapDispachToProps)(CotizarScreen);