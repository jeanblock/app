import React, {useState} from 'react';
import { View, Text, StyleSheet , TouchableOpacity, Modal, FlatList ,Image} from 'react-native';
import {Button} from 'react-native-paper';
import {getProductsSubcategory, getProductsAplications} from '../../apis/productsAPI';

export default function SubCategoriesFilter(props){

    const [show, setShow] = useState(false);
    const [select, setSelect] = useState("");


    function hableSelect(){
        getProductsSubcategory(props.category, select)
        .then(data => {
            props.action(data, select);
            setShow(false)
        }).catch(e => {
            console.log(e);
        });
        getProductsAplications(props.category, select).then( data => {
            props.getApplication(data)
        });
    };

    return(
        <>
        <TouchableOpacity style={Styles.container} onPress={() => setShow(true) } >
            <Text style={Styles.text}>Subcategoría</Text>
        </TouchableOpacity>
        <Modal
            visible={show}
            animationType="slide"
            transparent={true}
        >
            <View style={ Styles.contenModal } >
                <View style={Styles.modalInfo}>
                    <Text style={Styles.title} >
                        Selecciona una opción
                    </Text>
                    <TouchableOpacity onPress={() => setShow(false)} style={Styles.close}>
                        <Image
                            style={Styles.icon}
                            source={require('../../assets/icons/cancel.png')}
                        />
                    </TouchableOpacity>

                    <FlatList
                        keyExtractor={ (item, index) => index.toString() }
                        data={props.info}
                        renderItem={ ({item}) => {
                            return (
                            <TouchableOpacity style={[Styles.item,
                                { backgroundColor: select == item.subcategory? "#EAEAEA" : "#FFF" } ]}
                                onPress={ () => setSelect(item.subcategory) }>
                                <Text style={{ fontSize: 17, margin: 2 }} > { item.subcategory} </Text>
                            </TouchableOpacity>
                            )}
                        }
                    />
                    <Button mode="contained"
                        style={ Styles.btn}
                        onPress={hableSelect}
                    >
                        Filtrar
                    </Button>
                </View>
            </View>
        </Modal>
        </>
    )
};
const Styles = StyleSheet.create({
    container: {
        backgroundColor: '#F1F0F0',
        width: '31%',
        marginHorizontal: 4, height: 40, justifyContent: 'center',
        padding: 0, borderRadius: 45, elevation: 1
    },
    text: {
        fontSize: 14,
        textAlign: 'center'
    },
    contenModal:{
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: '#2424243b'
    },
    title: {
        fontSize: 20,
        marginBottom: 16,
        fontWeight: '700'
    },
    modalInfo:{
        backgroundColor: '#fff',
        height: '60%',
        borderTopLeftRadius: 16, borderTopRightRadius: 16,
        padding: 16
    },
    item: {
        padding: 10, marginBottom: 10, borderRadius: 50
    },
    close: {
        position: 'absolute', right: 20, top: 15 ,
        padding: 5 
    },
    icon: {
        width: 20,
        height: 20
    },
    btn: {
        marginBottom: 20, borderRadius: 50,
        backgroundColor: "#eb0a1e"
    }
});