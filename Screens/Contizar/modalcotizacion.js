import React from 'react';
import { View , Text,  Modal} from 'react-native';
import { Button} from 'react-native-paper';
import {red} from '../../theme/vars';
export default function ModalCotizacion(props){
    return(
        <Modal animationType="fade"
            visible={props.visible}
            transparent={true}
        >
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center' , 
                backgroundColor: '#21212194', paddingTop: 100, padding: 16
            }}>
                <View style={{
                    backgroundColor: "#fff",
                    width: '100%',
                    paddingVertical: 40 ,
                    borderRadius: 20, justifyContent: 'space-between', alignItems: 'center'
                }}>
                    <View style={{ width: '100%' }}>
                        <Text style={{ fontWeight: '600', fontSize: 25, marginBottom: 10,
                            textAlign: 'center'
                        }} >
                            ¡Cotizacion Enviada!
                        </Text>
                        <Text style={{ fontWeight: '400', fontSize: 18, marginBottom: 10,   textAlign: 'center' }}>
                            ¡Muchas gracias! Pronto un asesor se estará contactando contigo para confirmar tu solicitud”
                        </Text>
                    </View>

                    <Button mode="contained" color={red}
                        style={{ marginTop: 20 , width: '80%', borderRadius: 20}}
                        onPress={()=> props.hiden(false)}
                    >
                        ENTIENDO
                    </Button>
                </View>
            </View>
        </Modal>
    )
}