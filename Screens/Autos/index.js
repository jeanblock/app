import React, { useState, useEffect } from 'react';
import { View , Text, FlatList,  SafeAreaView, Modal } from 'react-native';
import { Button, ActivityIndicator } from 'react-native-paper';
import ProfileComponents from '../../components/profile';
import CarItem from '../../components/cars/cardItem';
import {titles, subtitle, red} from '../../theme/vars';
import { connect } from 'react-redux';
import {getVehicleActions} from '../../Redux/Actions/getVehicle.action'

const AutosScreen = (props) => {
    async function update(){
        await props.getVehicleActions();
    };
    useEffect( () => {
        update()
    }, [] );

    return (
        <SafeAreaView style={{ flex: 1 }}>

        <View style={{ flex: 1 }}>
            <View style={{ height: 114, borderBottomLeftRadius: 20, borderBottomRightRadius: 20,
                overflow: 'hidden' , backgroundColor: "#fff", marginBottom: 20}}>
                <ProfileComponents
                    navigate={props.navigation}
                />
            </View>

            <View style={{ flex: 1, backgroundColor: "#fff", padding: 8 }}>
                <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between',
                    padding: 8, paddingBottom: 0
                }}>
                    <View style={{ width: "60%" }}>
                        <Text style={titles}>
                                Mis vehículos
                        </Text>
                    </View>
                    <View style={{ width: "40%" }}>
                        <Button
                            mode="contained"
                            color="#EB0A1E"
                            style={{ borderRadius: 50 }}
                            onPress={update}
                        >
                            Actualizar
                        </Button>
                    </View>

                </View>
                <Text style={[subtitle, { marginBottom: 10 }]}>
                    Haz click en 'Actualizar' para sincronizar tus vehículos actuales.
                </Text>
                { props.vehicleData.length >= 1  ? 
                    <FlatList
                        data={props.vehicleData}
                        keyExtractor={ (item) => item.id.toString() }
                        style={{ padding:  3 }}
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                        renderItem={ ({item}) => {
                            return <CarItem
                                    img={item.image_url}
                                    name={item.brand}
                                    placa={item.plate}
                                    vin={item.VIN}
                                    id={item.id}
                                    years={item.year}
                                    model={item.model}
                                />
                            }
                        }
                    />
                : 
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                    <ActivityIndicator animating={true} color={red}/>
                </View>
                }
            </View>
        </View>
        </SafeAreaView>
    );
};
const mapStateToProps = (state) => state;
const mapDispachToProps = {
    getVehicleActions
}
export default connect(mapStateToProps, mapDispachToProps)(AutosScreen);