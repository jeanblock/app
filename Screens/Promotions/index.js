import React from 'react';
import { View, Text, SafeAreaView, Image, ScrollView, TouchableOpacity, StyleSheet, Dimensions, Linking, ActivityIndicator } from 'react-native';
import { Modal, Portal, Button, Provider, IconButton } from 'react-native-paper';
import ProfileComponents from '../../components/profile';
import { titles, subtitle } from '../../theme/vars';
import Carousel from 'react-native-snap-carousel';
//import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { API } from '../../apis/const'
import AsyncStorage from '@react-native-community/async-storage';
import Moment from 'moment';
Moment.locale('es')

const { width, height } = Dimensions.get("screen")

class PromosScreen extends React.Component {

    constructor(props){
        super(props)

        this.state = {
            activeIndex: 0,
            carousel: null,
            news: [],
            promos: [],
            isActivePromoTab: true,
            isLoadingNews: true,
            isLoadingPromos: true,
            imageLoading: true
        }
    }

    _renderNewsItem = ({ item, index }) => {
        return (
            <View style={styles.renderItemContainer}>
                <Text style={styles.cardTitle}>{item.title}</Text>
                <Text style={styles.cardText}>{item.date}</Text>
                <Image
                    source={{ uri: item.picture }}
                    style={styles.cardImage}
                />
                <Button
                    mode="contained"
                    color="#FF0000"
                    onPress={() => Linking.openURL(item.url)}
                    style={{ marginVertical: 5, borderRadius: 40 }}
                >
                    Más información
                </Button>
            </View>
        )
    }

    ImageLoading_Error() {
        this.setState({ imageLoading: false });
    }

    _renderPromoItem = ({ item }) => {
        return (
            <View style={[styles.renderItemContainer, { marginBottom: 15, width: '100%' }]}>
                <Text style={styles.cardTitle}>{item.title}</Text>
                <View style={{ justifyContent: 'flex-start', flexDirection: 'row' }}>
                    <Text style={styles.cardText}>{item.description}</Text>
                </View>
                <Image
                    source={ this.state.imageLoading ?
                        { uri: item.image_url } : { uri: 'https://farm5.staticflickr.com/4363/36346283311_74018f6e7d_o.png' }}
                    onError={ this.ImageLoading_Error.bind(this) }
                    style={ styles.cardImage }
                />
                <Button
                    mode="contained"
                    color="#FF0000"
                    onPress={() => Linking.openURL(item.more_info_url) }
                    style={{ marginVertical: 5, borderRadius: 40 }}
                >
                    Más información
                </Button>
            </View>
        )
    }

    async getPromos(){

        this.setState({ isActivePromoTab: true })

        if ( this.state.promos.length == 0 ) {
            console.log('ENTRo en PROMOS')
            const token = await AsyncStorage.getItem('@tokens');

            await fetch(`${API}/api/v1/promotions`, {
                method: 'GET',
                headers : {
                    'Content-Type': 'application/json',
                    'Authorization': token,
                }
            })
            .then((res) => res.json())
            .then((data) => {
                this.setState({ promos: data.message })
                this.setState({ isLoadingPromos: false })
                console.log('PRMOS', this.state.promos)
            })
            .catch((err) => console.log(err))
        }

    }

    async getNews() {

        this.setState({ isActivePromoTab: false })

        if ( this.state.news.length == 0 ) {

            await fetch('http://test.toyotahonduras.451.com/api/news/').then((res) => res.json() )
            .then((data) => {
                this.setState({ news: data.noticias })
                this.setState({ isLoadingNews: false })
            })
            .catch((err) => console.log(err))
        }
    }

    componentDidMount() {
        this.getPromos()
    }

    render () {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView>
                    <View style={{ backgroundColor: "#585858" }}>

                        <View style={styles.buttonTabsContainer}>
                            <TouchableOpacity
                                style={[ styles.buttonTab, { backgroundColor: this.state.isActivePromoTab ? "#fff" : "#7d7d7d", }]}
                                onPress={ () => this.getPromos() }
                            >
                                <View style={ styles.tabContainer }>
                                 {/*    <Icon name="alert-decagram" size={24} color="#000" style={{ marginRight: 10 }} /> */}
                                    <Text style={styles.tabText}>Promociones</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={[styles.buttonTab2, { backgroundColor: !this.state.isActivePromoTab ? "#fff" : "#7d7d7d", }]}
                                onPress={ () => this.getNews() }>
                                <View style={ styles.tabContainer }>
                                    {/* <Icon name="email-alert" size={24} color="#000" style={{ marginRight: 10 }} /> */}
                                    <Text style={styles.tabText}>Noticias</Text>
                                </View>
                            </TouchableOpacity>
                        </View>

                        <View style={ styles.ProfileComponentContainer }>
                            <ProfileComponents navigate={this.props.navigation} />
                        </View>

                        { this.state.isActivePromoTab ? this.state.isLoadingPromos ?
                            (
                                <View style={{ flex: 1, height: height / 1.699, justifyContent: 'center' }}>
                                    <ActivityIndicator size="large" color="white" />
                                </View>
                            ) :
                            (
                                <View style={{ flex: 1 }}>
                                    <View style={{ marginBottom: 0, padding: 16 }}>
                                        <Text style={[titles, { color: "#fff" }]} >Promociones recientes</Text>
                                        <Text style={[subtitle, styles.subtitle]}>
                                            Conocé las promociones exclusivas para usuarios de nuestra aplicación móvil.
                                        </Text>
                                    </View>
                                    <View style={ styles.carouselContainer }>
                                        <Carousel
                                            layout={'stack'}
                                            ref={ref => this.state.carousel = ref}
                                            data={this.state.promos}
                                            sliderWidth={width}
                                            itemWidth={width / 1.18}
                                            renderItem={this._renderPromoItem}
                                            onSnapToItem={index => this.setState({ activeIndex: index })}
                                        />
                                    </View>
                                </View>
                            ) : // fin de la primera condicion
                            (
                            this.state.isLoadingNews ?
                            (
                                <View style={{ flex: 1, height: height / 1.699, justifyContent: 'center' }}>
                                    <ActivityIndicator size="large" color="white" />
                                </View>
                            ) :
                            (
                                <View style={{ flex: 1 }}>
                                    <View style={{ marginBottom: 0, padding: 16 }}>
                                        <Text style={[titles, { color: "#fff" }]} >Noticias recientes</Text>
                                        <Text style={[subtitle, styles.subtitle]}>
                                            Conocé las noticias para los usuarios de nuestra aplicación móvil.
                                        </Text>
                                    </View>
                                    <View style={ styles.carouselContainer }>
                                        <Carousel
                                            layout={'stack'}
                                            ref={ref => this.state.carousel = ref}
                                            data={this.state.news}
                                            sliderWidth={width}
                                            itemWidth={width / 1.18}
                                            renderItem={this._renderNewsItem}
                                            onSnapToItem={index => this.setState({ activeIndex: index })}
                                        />
                                    </View>
                                </View>
                            )
                        )}

                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    subtitle: {
        color: "#fff",
        fontSize: 18,
        padding: 0,
        paddingVertical: 10,
    },
    buttonTabsContainer: {
        flex: 0.1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: "#7d7d7d",
    },
    tabText: {
        fontSize: 18,
    },
    tabContainer: {
        width: '100%',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 13
    },
    ProfileComponentContainer: {
        height: 114,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        overflow: 'hidden',
        backgroundColor: "#fff",
        marginBottom: 10
    },
    carouselContainer: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10,
    },
    buttonTab: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 0 ,
        borderTopRightRadius: 30
    },
    buttonTab2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 0 ,
        padding: 3,
        backgroundColor: "#7d7d7d",
        borderTopLeftRadius: 30
    },
    renderItemContainer: {
        borderRadius: 15,
        padding: 12,
        paddingVertical: 20,
        backgroundColor: "#fff",
        width: width / 1.18,
    },
    cardTitle: {
        fontSize: 24,
        fontWeight: '600',
        color: "#454444",
        marginBottom: 10
    },
    cardImage: {
        width: "100%",
        height: 250,
        resizeMode: "cover",
        marginBottom: 10
    },
    cardText: {
        fontSize: 16,
        fontWeight: '300',
        color: "#454444",
        marginBottom: 10
    },
    boldText: {
        fontWeight: 'bold',
        fontSize: 16
    }
})


export default PromosScreen;