import React, { useState ,} from 'react';
import { View , SafeAreaView, StyleSheet, Image, TouchableOpacity} from 'react-native';
import { ActivityIndicator, Colors } from 'react-native-paper';
import { WebView } from 'react-native-webview';
import { connect } from 'react-redux';

const CitasScreen = (props) => {
   
    const [login, setLogin] = useState(true);
    const [size, setSize] = useState(1);
    const [url, setUrl] = useState(`https://parcial-corflo.cs77.force.com/CitasTaller?marca=10&bpc=${props.userData.user_sap_id}`)


    return (
        <SafeAreaView style={Style.container}>
            <View style={{flex: 0.1, flexDirection: 'row', justifyContent: 'space-between', backgroundColor: "#7d7d7d"}}>
                <TouchableOpacity  style={{ flex: 1, justifyContent: 'center', alignItems: 'center',borderRadius: 0 ,
                    backgroundColor: url === `https://parcial-corflo.cs77.force.com/CitasTaller?marca=10&bpc=${props.userData.user_sap_id}` ? "#fff" : "#7d7d7d",
                    borderTopRightRadius: 30
                }}
                onPress={ () => {
                    setLogin(true)
                    setUrl(`https://parcial-corflo.cs77.force.com/CitasTaller?marca=10&bpc=${props.userData.user_sap_id}`)
                    setSize(0)
                }}
                >
                    <Image source={require('../../assets/images/Toyota-logo.png')} style={{ width: '65%', height: 60 , resizeMode: 'stretch'}}/>
                </TouchableOpacity>

                <TouchableOpacity  style={{ flex: 1, justifyContent: 'center', alignItems: 'center',borderRadius: 0 ,
                    padding: 3,
                    backgroundColor: url === `https://parcial-corflo.cs77.force.com/CitasTaller?marca=12&bpc=${props.userData.user_sap_id}` ? "#fff" : "#7d7d7d",
                    borderTopLeftRadius: 30
                }}
 
                    onPress={ () => {
                        setLogin(true)
                        setUrl(`https://parcial-corflo.cs77.force.com/CitasTaller?marca=12&bpc=${props.userData.user_sap_id}`)
                        setSize(0)
                    }}
                >
                    <Image source={require('../../assets/images/logoFORD.png')} style={{ width: '90%', height: 55, resizeMode: 'stretch' }}/>
                </TouchableOpacity>
            </View>
            <View style={{ flex: 1 }}>

                {
                    login == true ? 
                    <View style={{ flex: 1, justifyContent: 'center', backgroundColor: "#fff", }} >
                        <ActivityIndicator color={Colors.red500 }/>
                    </View>
                    : null
                }

                <WebView source={{ uri: url }} style={{ flex: size}}
                    onLoadProgress={({ nativeEvent }) => {
                        if(nativeEvent.progress == 1 ){
                            setLogin(false)
                            setSize(1)
                        }
                    }}
                />
                
            </View>

        </SafeAreaView>
    );
};

const Style = StyleSheet.create({
    container : { flex: 1 },
    header: { height: 114, borderBottomLeftRadius: 20, borderBottomRightRadius: 20,
        overflow: 'hidden' , backgroundColor: "#fff", marginBottom: 20
    },
    body: { flex: 1, backgroundColor: "#fff",
        borderTopEndRadius: 20, borderTopLeftRadius: 20,
        padding: 16
    },
    fechaSelecConten :{
        flexDirection: 'row', justifyContent: 'flex-start',
        alignItems: 'center', backgroundColor: "#0096888a", paddingHorizontal: 15,
        paddingVertical: 15, borderRadius: 10
    },
    iconConten: {
        backgroundColor: '#009688', padding: 15, borderRadius: 10, marginRight: 30
    },
    icon:{
        width: 30, height: 30
    },
    btn:{
        padding: 8, paddingLeft: 0 , borderBottomWidth: 3 , borderBottomColor: "#6E6E6E"
    },
    contenItem:{ justifyContent: 'space-between', height: 50 },
    itemTitle: { fontSize: 20,
        color: "#fff", margin: 0, fontWeight: '800'
    },
    itemSubtitle: {
        fontSize: 16,
        color: "#fff", margin: 0, fontWeight: '800'
    },
    tipoCita :{
        marginVertical: 10, paddingLeft: 20 ,
        padding: 10, borderRadius: 10, backgroundColor: "#0096888a"
    },
    contenkms: {
        width: '50%',
        flexDirection: 'row',justifyContent: 'flex-start' ,
        alignItems: 'center',
    }
})

const mapStateToProps = state => state ;

export default connect(mapStateToProps, {})(CitasScreen);