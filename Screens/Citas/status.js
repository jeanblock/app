import React, { useState , useEffect} from 'react';
import { View , Text,  SafeAreaView, TouchableOpacity, ScrollView , Modal} from 'react-native';
import { TextInput, Button, Divider, List} from 'react-native-paper';

export default function StatusCita(props){
    return(
        <Modal animationType={'slide'} 
            visible={props.visible}
            transparent={true}
        >
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center' , 
                backgroundColor: '#21212194', paddingTop: 100, padding: 26
            }}>
                <View style={{
                    backgroundColor: "#fff",
                    width: '100%',
                    paddingVertical: 40 ,
                    borderRadius: 20, justifyContent: 'space-between', alignItems: 'center'
                }}>
                    <View style={{ width: '100%' }}>
                        
                        <Text style={{ fontWeight: '600', fontSize: 25, marginBottom: 10,
                            textAlign: 'center'
                        }} > 
                            ¡Tu cita fue agendada correctamente!
                        </Text>
                        
                        <Text style={{ fontWeight: '400', fontSize: 18, marginBottom: 10,   textAlign: 'center' }}>
                            Recibirás un correo de confirmación.
                        </Text>
                        
                        <List.Section 
                            style={{ marginLeft: 30, width: '80%'}}
                            
                        >
                            <List.Item 
                                title={props.hora}
                                description="hora"
                            />
                            <Divider />
                            <List.Item 
                                title={props.date}
                                description="Fecha"
                            />
                            <Divider />
                        </List.Section>
                    </View>
                                
                    <Button mode="contained" color="blue" 
                        style={{ marginTop: 20 , width: '80%', borderRadius: 20}}
                        onPress={()=> props.hiden(false)}
                    >
                        Entiendo
                    </Button>
                </View>
            </View>  
        </Modal>
    )
}