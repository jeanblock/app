import AsyncStorage from '@react-native-community/async-storage';
import React, { useState, useEffect } from 'react';
import { View, Text, Image } from 'react-native';
import { connect } from 'react-redux';
import auth from '@react-native-firebase/auth';

function Redirect (props){
    const getData = async () => {
        try {
          const value = await AsyncStorage.getItem('@tokens')
          console.log(value)
          if(value !== null) {
            props.navigation.navigate("Home");
          }else{
            props.navigation.navigate("welcome");
          }
        } catch(e) {
            console.log(e);
        }
    };

    useEffect( ()=> {
        getData();
        auth().signInAnonymously().then(() => {
          console.log('User signed in anonymously');
        })
        .catch(error => {
          console.log('error -> ', error);
        });

    } , [] );

    return (
        <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1,
          backgroundColor: '#ffffff'
          }} >
            <Image
                source={ require('../assets/splash.png') }
                style={{ width: 400 , height: 600, resizeMode: 'cover'}}
              />
        </View>
    )
};
const mapStateToProps = (state) => state;
export default connect(mapStateToProps, {})(Redirect);