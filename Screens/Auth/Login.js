import React, { useState} from 'react';
import { View, Text , SafeAreaView, Image , Alert, TouchableOpacity, Linking} from 'react-native';
import { TextInput , Button, Modal, Portal, Provider, ActivityIndicator, Colors } from 'react-native-paper';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { theme, red} from '../../theme/vars';
import { connect } from 'react-redux';
import { loginActions } from '../../Redux/Actions/login.action';
import {loginAPI} from '../../apis/loginApi';
import {dataUserActions} from '../../Redux/Actions/userData.action';

function LoginScreen(props){


    const [text, setText] = useState('');
    const [pass, setPass] = useState('');
    const [disable, setDisable ] = useState(true);
    const [passValid, setPassValid] = useState(true);
    const [error, setError] = useState(false);
    const [errorMesaage, setErrorMessage ] = useState("");

    const [visible, setVisible] = React.useState(false);

    const showModal = () => setVisible(true);

    const hideModal = () => setVisible(false);

    const validate = (text, type) => {

        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(type == "email"){
            if (reg.test(text) === false) {
                setText(text)
                setDisable(true)
              return false;
            }
            else {
                console.log("valido")
              setText(text);
              setDisable(false)
            }
        } else if(type == "pass"){
            setPass(text);
             text.length >= 8 ?  setPassValid(false) : setPassValid(true)
        }
    }
    function warning (params) {
        Alert.alert(
            "Ups",
            params,
            [
              { text: "OK", onPress: () => console.log("OK Pressed") }
            ],
            { cancelable: false }
        );
    };

    async function loginFunctions(){
        let data = {
            "email": text,
            "password": pass
        }
        const info = await loginAPI(data);

        if(info.code == 200){
            props.loginActions(info.message.Id, info.message.Authorization);
            props.dataUserActions()
            props.navigation.navigate("Home");
            hideModal();
        }else if(info.code == 400){
            setError(true);
            setErrorMessage(info.message)
        }
        //props.loginActions(data);
    }
    console.log(props);
    return(
        <SafeAreaView style={{ flex: 1 }}>
            <KeyboardAwareScrollView style={{ height: '100%' }}>
                <View style={{ flex: 1 }}>
                    <View style={{ height: 120 , backgroundColor: "#fff",
                        borderBottomRightRadius: 40, borderBottomLeftRadius: 40,
                        marginBottom: 20, justifyContent: 'center', alignItems: 'center'
                    }}
                    >
                        <Image
                            source={ require('../../assets/images/app-brands.png') }
                            style={{ width: "100%", margin: 0, resizeMode: 'contain' }}
                        />
                    </View>

                    <View style={{ flex: 1 , padding: 16}}>
                        <View style={{ flex: 1 , backgroundColor: "#fff",
                        borderRadius: 20 , paddingBottom: 40,
                        padding: 16}}>
                            <Text style={{fontSize: 22, fontWeight: '700', textAlign: 'center'}}>
                                Ingresa con tu cuenta
                            </Text>
                            <TextInput
                                label="Correo"
                                autoCapitalize="none"
                                value={text}
                                keyboardType={'email-address'}
                                theme={theme}
                                onChangeText={text => validate(text, "email")}
                                style={{ marginVertical: 10, backgroundColor: "#fff" }}
                            />
                            <TextInput
                                label="Contraseña"
                                value={pass}
                                autoCapitalize="none"
                                secureTextEntry={true}
                                theme={theme}
                                onChangeText={text => validate(text, "pass")}
                                style={{ marginVertical: 10 , backgroundColor: "#fff"}}
                            />
                            <Button mode="text"
                            color="#3498db"
                            style={{ width: 190, marginBottom: 40, padding: 0 }}
                            onPress={() => console.log('Pressed')}
                            >
                            <TouchableOpacity
                                onPress={()=>   Linking.openURL("https://appgrupoflores.com/olvide.html") }
                                >
                                <Text style={{ fontSize: 15, textAlign: 'left', textTransform: 'none', marginTop: 10 }}>
                                    Olvidé la contraseña
                                </Text>
                            </TouchableOpacity>

                            </Button>
                            <Button
                                color={red}
                                mode="contained"
                                onPress={() => {

                                    if(disable == false && passValid == false ){
                                        showModal();
                                        return loginFunctions();
                                    }else if(disable == true){
                                        return warning("Ingresa un correo electrónico válido.")
                                    }else if(passValid == true){
                                        return warning("La contraseña ingresada es muy corta.")
                                    }
                                }}>
                                Ingresar
                            </Button>
                            <View style={{ flexDirection: 'row', marginTop: 30 , justifyContent: 'center'}}>
                                <Text style={{ fontSize: 18 }}> ¿No tienes cuenta? </Text>
                                <TouchableOpacity
                                    onPress={()=> props.navigation.navigate("Register")}
                                >
                                    <Text style={{ fontSize: 18, textTransform: 'none', color: '#3498db' }}>
                                        Regístrate gratis
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </KeyboardAwareScrollView>
                <Provider>
                    <Portal >
                        <Modal visible={visible} dismissable={false}
                            style={{ padding: 16,  }}
                        >
                            <View  style={{ backgroundColor: "#fff", marginHorizontal: 16,
                                borderRadius: 16, padding: 26, justifyContent: 'center', alignItems: 'center',
                            }}>
                                <Text style={{ fontSize: 20, fontWeight: '700', marginBottom: 30 }}>
                                  {error == false ? "Verificando tus datos..." : errorMesaage }
                                </Text>
                                {error == false ?
                                    <ActivityIndicator animating={true} color={Colors.red800}
                                        size={80}
                                    />
                                    : <Button mode="contained" color={Colors.red800}
                                        onPress={()=> {
                                            hideModal();
                                            setError(false)
                                        }}
                                    >
                                        Reintentar
                                    </Button>
                                }
                            </View>
                        </Modal>
                    </Portal>
                </Provider>
        </SafeAreaView>
    )
};
const mapStateToProps = (state) => state
const mapDispatchToProps = {
    loginActions, loginActions,
    dataUserActions
}
export default connect( mapStateToProps , mapDispatchToProps )(LoginScreen);