import React, { useState, useEffect } from 'react';
import { View, Text , SafeAreaView, Image , Alert, TouchableOpacity} from 'react-native';
import { TextInput , Button, Modal, Portal, Provider, ActivityIndicator, Colors } from 'react-native-paper';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { theme, red} from '../../theme/vars';
import { connect } from 'react-redux';
import { loginActions } from '../../Redux/Actions/login.action';
import {loginAPI} from '../../apis/loginApi';

function RecoveryScreen(props){

    const [text, setText] = useState('');

    async function loginFunctions(){
        let data = {
            "email": text
        }
       console.log(data )
        //props.loginActions(data);
    }
    return(
        <SafeAreaView style={{ flex: 1 }}>
            <KeyboardAwareScrollView style={{ height: '100%' }}>
                <View style={{ flex: 1 }}>
                    <View style={{ height: 120 , backgroundColor: "#fff",
                        borderBottomRightRadius: 40, borderBottomLeftRadius: 40,
                        marginBottom: 20, justifyContent: 'center', alignItems: 'center'
                    }}
                    >
                        <Image
                            source={ require('../../assets/images/app-brands.png') }
                            style={{ width: "100%", margin: 0, resizeMode: 'contain' }}
                        />
                    </View>

                    <View style={{ flex: 1 , padding: 16}}>
                        <View style={{ flex: 1 , backgroundColor: "#fff",
                        borderRadius: 20 , paddingBottom: 40,
                        padding: 16}}>
                            <Text style={{fontSize: 22, fontWeight: '700', textAlign: 'center'}}>
                                Recupera tu contraseña
                            </Text>
                            <TextInput
                                label="Correo"
                                autoCapitalize="none"
                                value={text}
                                keyboardType={'email-address'}
                                theme={theme}
                                onChangeText={text => validate(text, "email")}
                                style={{ marginVertical: 10, backgroundColor: "#fff" , marginBottom: 40}}
                            />
                            <Button
                                color={red}
                                mode="contained"
                            >
                                Enviar
                            </Button>
                            <View style={{ flexDirection: 'row', marginTop: 30 , justifyContent: 'center'}}>
                                <Text style={{ fontSize: 18 }}> ¿No tienes cuenta? </Text>
                                <TouchableOpacity
                                    onPress={()=> props.navigation.navigate("Register")}
                                >
                                    <Text style={{ fontSize: 18, textTransform: 'none', color: '#3498db' }}>
                                        Regístrate gratis
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </KeyboardAwareScrollView>
        </SafeAreaView>
    )
};
const mapStateToProps = (state) => state 
const mapDispatchToProps = {
    loginActions
}
export default connect( mapStateToProps , mapDispatchToProps )(RecoveryScreen);