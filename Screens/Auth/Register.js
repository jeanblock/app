import React, { useState, useEffect } from 'react';
import { View, Text , SafeAreaView,TouchableOpacity, Alert, Image} from 'react-native';
import { TextInput , Button, Provider, Portal, Modal, Colors, ActivityIndicator , Divider} from 'react-native-paper';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { theme , red} from '../../theme/vars';
import {registerAPI} from '../../apis/registerApi';
import RNPickerSelect from 'react-native-picker-select';
import DateTimePicker from '@react-native-community/datetimepicker';

function Register(props){
    const [name, setName] = useState('');
    const [last_name, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [national_id, setNationalId] = useState('');
    const [pass, setPass] = useState('');
    const [phone, setPhone] = useState('');
    const [disable, setDisable] = useState(true);
    const [passValid , setPassValid] = useState(true);
    const [visible, setVisible] = useState(false);
    const [error, setError] = useState(false);
    const [erroMsj, setErroMsj] = useState("");
    const [stadoCivil, setStadoCivil] = useState("");
    const [ocupacion, setOcupacion] = useState("");
    const [genders, setGenders] = useState({ language: 'java', })
    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');




    function validate(text, type){

        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(type == "email"){
            if (reg.test(text) === false) {
                setEmail(text)
                setDisable(true)
              return false;
            }
            else {
   
                setEmail(text);
                setDisable(false)
            }
        } else if(type == "pass"){
            setPass(text);
            text.length >= 8 ?  setPassValid(false) : setPassValid(true)
        }
    }
    function warning (params) {
        Alert.alert(
            "¡Han ocurrido errores!",
            params,
            [
              { text: "OK", onPress: () => console.log("OK Pressed") }
            ],
            { cancelable: false }
        );
    };
    useEffect( () => {
        Alert.alert(
            "Estimado(a) Usuario",
            "Para hacer uso de nuestra aplicación, usted ya debe ser cliente de Toyota o de Ford.",
            [
              {
                text: "Cancel",
                onPress: () => props.navigation.navigate("welcome"),
                style: "cancel"
              },
              { text: "OK", onPress: () => console.log("OK Pressed") }
            ],
            { cancelable: false }
        );
    } , [] )
    async function registerFunction(){
        
        setVisible(true);
        if(disable == true && passValid == true ){
            warning("Valida tus datos antes de continuar.")
        }else if(name != "" && last_name != "" && national_id != "" && email != "" && pass != "" && phone != "" ){
            let obj = {
                "first_name": name,
                "last_name": last_name,
                "identification_number": national_id,
                "email": email,
                "password": pass,
                "phone": phone
            };
            const info = await registerAPI(obj)
            if(info.status == "error") {
                setError(true);
                setErroMsj(info.message)
            }else if( info.status == "success" ){
                props.navigation.navigate("Login")
            }
            // 
        }else{
            warning("Valida tus datos antes de continuar.")
        }
        
    }

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
       
        setDate(currentDate);
    };
    
    
    
    console.log(date);



    return(
        <SafeAreaView style={{ flex: 1 }} >
            <KeyboardAwareScrollView style={{ height: '100%' }}>
            <View style={{ flex: 1 }}>
            <View style={{ height: 120 , backgroundColor: "#fff",
                    borderBottomRightRadius: 40, borderBottomLeftRadius: 40,
                    marginBottom: 10, justifyContent: 'center', alignItems: 'center'
                }}
                >
                    <Image
                        source={ require('../../assets/images/app-brands.png') }
                        style={{ width: "100%", margin: 0, resizeMode: 'contain' }}
                    />
            </View>

            <View style={{ flex: 1 , padding: 16}}>
                <View style={{ flex: 1 , backgroundColor: "#fff",
                    borderRadius: 20 , paddingBottom: 70,
                    padding: 16}}>
                    <Text style={{fontSize: 22, fontWeight: '700', textAlign: 'center'}}>
                        Crea tu cuenta gratis
                    </Text>
                <View style={{ flex: 1 , backgroundColor: "#fff"}}
                >
                    <TextInput
                        label="Nombre *"
                        autoCapitalize="none"
                        theme={theme}
                        value={name}
                        onChangeText={text => setName(text)}
                        style={{ marginVertical: 10, backgroundColor: "#fff" }}
                    />
                    <TextInput
                        label="Apellido *"
                        autoCapitalize="none"
                        theme={theme}
                        value={last_name}
                        onChangeText={text => setLastName(text)}
                        style={{ marginVertical: 10, backgroundColor: "#fff" }}
                    />
                    <TextInput
                        label="Número de identificación *"
                        autoCapitalize="none"
                        keyboardType="numeric"
                        theme={theme}
                        value={national_id}
                        keyboardType={'numeric'}
                        onChangeText={text => setNationalId(text)}
                        style={{ marginVertical: 10, backgroundColor: "#fff" }}
                    />
                    <TextInput
                        label="Correo electrónico *"
                        autoCapitalize="none"
                        value={email}
                        placeholder="Ejemplo: juanperez@gmail.com"
                        theme={theme}
                        keyboardType={'email-address'}
                        onChangeText={text => validate(text, "email")}
                        style={{ marginVertical: 10 , backgroundColor: "#fff"}}
                    />
                    <TextInput
                        label="Contraseña *"
                        autoCapitalize="none"
                        value={pass}
                        secureTextEntry={true}
                        theme={theme}
                        placeholder="Debe tener mínimo 8 caracteres"
                        onChangeText={text => validate(text, "pass")}
                        style={{ marginVertical: 10 , backgroundColor: "#fff"}}
                    />
                    <TextInput
                        keyboardType="numeric"
                        autoCapitalize="none"
                        label="Teléfono"
                        theme={theme}
                        value={phone}
                        keyboardType={'phone-pad'}
                        onChangeText={text => setPhone(text)}
                        style={{ marginVertical: 10 , backgroundColor: "#fff"}}
                    />
                    
                      <View style={{ padding: 8, justifyContent: 'center', marginTop: 16 }}> 
                        <Text  style={{ marginBottom: 10, marginTop: 10, marginLeft: 0 ,fontSize: 18, color: "#afafaf"}}>
                            Ocupación
                        </Text>
                        <RNPickerSelect
                            onValueChange={(value) => console.log(value)}
                            placeholder={{
                                label: 'Click para selecionar una opcion',
                                value: null,
                            }}
                            placeholderTextColor="#afafaf"
                            style={{fontSize: 16}}
                            items={[
                               
                                { label : "ABOGADO", value: "ABOGADO"},
                                { label: "ACUICULTOR", value: "ACUICULTOR"},
                                { label: "ADMINISTRACION DE NEGOCIOS INTERNACIONALES", value: "ADMINISTRACION DE NEGOCIOS INTERNACIONALES"},
                                { label: "AGRICULTOR", value: "AGRICULTOR"},
                                { label: "AMA DE CASA", value: "AMA DE CASA"},
                                { label: "ANTROPOLOGIA", value: "ANTROPOLOGIA"},
                                { label: "ARQUITECTURA", value: "ARQUITECTURA"},
                                { label: "ARTE", value: "ARTE"},
                                { label: "BACHILLER", value: "BACHILLER"},
                                { label: "CAFICULTOR", value: "CAFICULTOR"},
                                { label: "CARPINTERO", value: "CARPINTERO"},
                                { label: "CATADOR DE CAFE", value: "CATADOR DE CAFE"},
                                { label: "CHEF", value: "CHEF"},
                                { label: "CIENCIA POLITICA", value: "CIENCIA POLITICA"},
                                { label: "CIENCIAS BIOLOGICAS", value: "CIENCIAS BIOLOGICAS"},
                                { label: "COCINERO", value: "COCINERO"},
                                { label: "COMERCIANTE INDIVIDUAL", value: "COMERCIANTE INDIVIDUAL"},
                                { label: "COMUNICACIÓN SOCIAL (PERIODISTA)", value: "COMUNICACIÓN SOCIAL (PERIODISTA)"},
                                { label: "DOCENTE", value: "DOCENTE"},
                                { label: "EBANISTA", value: "EBANISTA"},
                                { label: "FONTANERO", value: "FONTANERO"},
                                { label: "GANADERO", value: "GANADERO"},
                                { label: "INGENIERIA AGROFORESTAL", value: "INGENIERIA AGROFORESTAL"},
                                { label: "INGENIERIA AGROINDUSTRIAL", value: "INGENIERIA AGROINDUSTRIAL"},
                                { label: "INGENIERIA ELECTRICA", value: "INGENIERIA ELECTRICA"},
                                { label: "INGENIERIA ELECTRONICA", value: "INGENIERIA ELECTRONICA"},
                                { label: "INGENIERIA EN INFORMATICA", value: "INGENIERIA EN INFORMATICA"},
                                { label: "INGENIERIA EN QUIMICA", value: "INGENIERIA EN QUIMICA"},
                                { label: "INGENIERIA INDUSTRIAL", value: "INGENIERIA INDUSTRIAL"},
                                { label: "INGENIERÍA MECÁNICA", value: "INGENIERÍA MECÁNICA"},
                                { label: "INGENIERO AGRONOMO", value: "INGENIERO AGRONOMO"},
                                { label: "INGENIRIA CIVIL", value: "INGENIRIA CIVIL"},
                                { label: "LICENCIADO ADMINISTRACIÓN DE NEGOCIOS INTERNACIONALES", value: " LICENCIADO ADMINISTRACIÓN DE NEGOCIOS INTERNACIONALES"},
                                { label: "LICENCIADO EN ANTROPOLOGÍA", value: "LICENCIADO EN ANTROPOLOGÍA"},
                                { label: "LICENCIADO EN BIOLOGÍA", value: "LICENCIADO EN BIOLOGÍA"},
                                { label: "LICENCIADO EN CIENCIAS NATURALES", value: "LICENCIADO EN CIENCIAS NATURALES"},
                                { label: "LICENCIADO EN COMERCIO INTERNACIONAL", value: "LICENCIADO EN COMERCIO INTERNACIONAL"},
                                { label: "LICENCIADO EN CONTADURIA", value: "LICENCIADO EN CONTADURIA"},
                                { label: "LICENCIADO EN ECONOMÍA", value: "LICENCIADO EN ECONOMÍA"},
                                { label: "LICENCIADO EN ECOTURISMO", value: "LICENCIADO EN ECOTURISMO"},
                                { label: "LICENCIADO EN EDUCACIÓN FÍSICA", value: "LICENCIADO EN EDUCACIÓN FÍSICA"},
                                { label: "LICENCIADO EN ENFERMERIA", value: "LICENCIADO EN ENFERMERIA"},
                                { label: "LICENCIADO EN FILOSOFIA", value: "LICENCIADO EN FILOSOFIA"},
                                { label: "LICENCIADO EN FINANZAS", value: "LICENCIADO EN FINANZAS"},
                                { label: "LICENCIADO EN FISICA", value: "LICENCIADO EN FISICA"},
                                { label: "LICENCIADO EN HISTORIA", value: "LICENCIADO EN HISTORIA"},
                                { label: "LICENCIADO EN LENGUAS EXTRANJERAS", value: "LICENCIADO EN LENGUAS EXTRANJERAS"},
                                { label: "LICENCIADO EN MATEMATICAS", value: "LICENCIADO EN MATEMATICAS"},
                                { label: "LICENCIADO EN MICROBIOLOGIA", value: "LICENCIADO EN MICROBIOLOGIA"},
                                { label: "LICENCIADO EN MÚSICA", value: "LICENCIADO EN MÚSICA"},
                                { label: "LICENCIADO EN PEDAGOGÍA", value: "LICENCIADO EN PEDAGOGÍA"},
                                { label: "LICENCIADO EN PSICOLOGIA", value: "LICENCIADO EN PSICOLOGIA"},
                                { label: "LICENCIADO EN QUIMICA Y FARMACIA", value: "LICENCIADO EN QUIMICA Y FARMACIA"},
                                { label: "LICENCIADO EN SOCIOLOGIA", value: "LICENCIADO EN SOCIOLOGIA"},
                                { label: "LICENCIADO EN TRABAJO SOCIAL", value: "LICENCIADO EN TRABAJO SOCIAL"},
                                { label: "LICENCIADO EN TURISMO", value: "LICENCIADO EN TURISMO"},
                                { label: "LICENCIATURA EN ADMINISTRACION DE EMPRESAS", value: "LICENCIATURA EN ADMINISTRACION DE EMPRESAS"},
                                { label: "LICENCIATURA EN ENFERMERIA", value: "LICENCIATURA EN ENFERMERIA"},
                                { label: "LICENCIATURA EN GERENCIA DE NEGOCIOS", value: "LICENCIATURA EN GERENCIA DE NEGOCIOS"},
                                { label: "LICENCIATURA EN MERCADOTECNIA", value: "LICENCIATURA EN MERCADOTECNIA"},
                                { label: "LICENCIATURA EN NUTRICION", value: "LICENCIATURA EN NUTRICION"},
                                { label: "MAESTRO DE OBRAS", value: "MAESTRO DE OBRAS"},
                                { label: "MARINO", value: "MARINO"},
                                { label: "MECÁNICO AUTOMOTRIZ", value: "MECÁNICO AUTOMOTRIZ"},
                                { label: "MEDICO ESPECIALISTA", value: "MEDICO ESPECIALISTA"},
                                { label: "MEDICO GENERAL", value: "MEDICO GENERAL"},
                                { label: "MÚSICO", value: "MÚSICO"},
                                { label: "ODONTOLOGÍA", value: "ODONTOLOGÍA"},
                                { label: "OFICIAL DE POLICÍA", value: "OFICIAL DE POLICÍA"},
                                { label: "PASTOR", value: "PASTOR"},
                                { label: "PESCADOR", value: "PESCADOR"},
                                { label: "PINTOR", value: "PINTOR"},
                                { label: "SACERDOTE", value: "SACERDOTE"},
                                { label: "SECRETARIA", value: "SECRETARIA"},
                                { label: "TÉCNICO EN ALIMENTOS Y BEBIDAS", value: "TÉCNICO EN ALIMENTOS Y BEBIDAS"},
                                { label: "TÉCNICO EN AVIACIÓN", value: "TÉCNICO EN AVIACIÓN"},
                                { label: "TÉCNICO EN DISEÑO GRÁFICO", value: "TÉCNICO EN DISEÑO GRÁFICO"},
                                { label: "TÉCNICO EN ELECTRICIDAD", value: "TÉCNICO EN ELECTRICIDAD"},
                                { label: "TÉCNICO EN LENGUAS DE SEÑAS", value: "TÉCNICO EN LENGUAS DE SEÑAS"},
                                { label: "TÉCNICO EN MICROBIOLOGÍA Y PARASITOLOGÍA", value: "TÉCNICO EN MICROBIOLOGÍA Y PARASITOLOGÍA"},
                                { label: "TÉCNICO EN MICROFINANZAS", value: "TÉCNICO EN MICROFINANZAS"},
                                { label: "TÉCNICO EN RADIOLOGÍA", value: "TÉCNICO EN RADIOLOGÍA"},
                                { label: "TÉCNICO EN TERAPIA FÍSICA Y REHABILITACIÓN", value: "TÉCNICO EN TERAPIA FÍSICA Y REHABILITACIÓN"},
                                { label: "TÉCNICO EN TERAPIA OCUPACIONAL", value: "TÉCNICO EN TERAPIA OCUPACIONAL"},
                                { label: "TRANSPORTISTA", value: "TRANSPORTISTA"},
                                { label: "VETERINARIO", value: "VETERINARIO"},
                            ]}
                           
                        />
                        <Divider style={{ backgroundColor: "#afafaf" }}/>
                    </View>
                    <View style={{ padding: 8, justifyContent: 'center', marginTop: 16 }}> 
                        <Text  style={{ marginBottom: 10, marginTop: 10, marginLeft: 0 ,fontSize: 18, color: "#afafaf"}}>
                            Género
                        </Text>
                        <RNPickerSelect
                            onValueChange={(value) => console.log(value)}
                            placeholder={{
                                label: 'Click para selecionar una opcion',
                                value: null,
                            }}
                            placeholderTextColor="#afafaf"
                            style={{fontSize: 16}}
                            items={[
                                { label: 'Masculino', value: 'Masculino' },
                                { label: 'Femenino', value: 'Femenino' },
                                
                            ]}
                           
                        />
                        <Divider style={{ backgroundColor: "#afafaf" }}/>
                    </View>
                       
                   <View style={{ padding: 8, justifyContent: 'center', marginTop: 16 }}>
                    <Text  style={{ marginBottom: 10, marginTop: 10, marginLeft: 0 ,fontSize: 18, color: "#afafaf"}}>
                            Estado Civil
                        </Text>
                        <RNPickerSelect
                            onValueChange={(value) =>  setStadoCivil(value)}
                            placeholder={{
                                label: 'Click para selecionar una opcion',
                                value: null,
                            }}
                            placeholderTextColor="#afafaf"
                            style={{fontSize: 16}}
                            items={[
                                { label: 'Casado/a', value: 'Casado/a' },
                                { label: 'Divorciado/a', value: 'Divorciado/a' },
                                { label: 'Separado/a', value: 'Separado/a' },
                                { label: 'Soltero/a', value: 'Soltero/a' },
                                { label: 'Viudo/a', value: 'Viudo/a' },
                            ]}
                            
                        />
                         <Divider style={{ backgroundColor: "#afafaf" }}/>
                   </View>
                    

                    <View>
                        <Text  style={{ marginBottom: 10, marginTop: 10, marginLeft: 8 ,fontSize: 18, color: "#afafaf"}}>Fecha de Nacimiento</Text>
                        <DateTimePicker
                            testID="dateTimePicker"
                            value={date}
                            mode={mode}
                            is24Hour={true}
                            display="default"
                            onChange={onChange}
                            style={{ backgroundColor: "#fff" }}
                        /> 
                    </View>
                </View>
                <View style={{ marginTop: 20 }}>
                    <Button
                        color={red}
                        mode="contained" onPress={registerFunction}>
                        Continuar
                    </Button>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 30 , justifyContent: 'center'}}>
                    <Text style={{ fontSize: 18 }}>¿Ya tienes cuenta? </Text>
                    <TouchableOpacity
                        onPress={()=> props.navigation.navigate("Login")}
                    >
                        <Text style={{ fontSize: 18, textTransform: 'none', color: '#3498db' }}>
                            Ingresa aquí
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
            </View>
            </View>
            </KeyboardAwareScrollView>
            <Provider>
                <Portal>
                    <Modal visible={visible} dismissable={false}
                        style={{ padding: 16,  }}
                    >
                        <View  style={{ backgroundColor: "#fff", marginHorizontal: 16,
                            borderRadius: 16, padding: 26, justifyContent: 'center', alignItems: 'center',
                        }}>
                            <Text style={{ fontSize: 20, fontWeight: '700', marginBottom: 30 }}>
                                {error == false ? "Verificando tus datos..." : erroMsj }
                            </Text>
                            {error == false ?
                                <ActivityIndicator animating={true} color={Colors.red800}
                                    size={80}
                                />
                                : <Button mode="contained" color={Colors.red800}
                                    onPress={()=>  {
                                        setVisible(false);
                                        setError(false)
                                    }}
                                >
                                    Reintentar
                                </Button>
                            }
                        </View>
                    </Modal>
                </Portal>
            </Provider>
        </SafeAreaView>
    )
};
export default Register;