import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Register from './Screens/Auth/Register';
import LoginScreen from './Screens/Auth/Login';
import WelcomeScree from './Screens/welcome';
import TabNavigation from './Navigations/tab';
import reduxThunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import {Provider} from 'react-redux';
import combineReducers from './Redux/index';
import Profile from './Screens/Profile';
import Redirect from './Screens/Redirect';
import RecoveryScree from './Screens/Auth/Recovery';

const store = createStore(combineReducers, {}, applyMiddleware(reduxThunk));
const Stack = createStackNavigator();

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
         
          <Stack.Screen name="inital" component={Redirect} options={{ headerShown: false }} />
          <Stack.Screen name="welcome" component={ WelcomeScree} options={{ headerShown: false }} />
          <Stack.Screen name="Login" component={LoginScreen} options={{ headerShown: false }}/>
          <Stack.Screen name="Register" component={Register} options={{ headerShown: false }}/>
          <Stack.Screen name="recovery" component={RecoveryScree} options={{ headerShown: false }}/>
          <Stack.Screen name="Home" component={TabNavigation} options={{ headerShown: false }}/>
          <Stack.Screen name="Profile" component={Profile} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};
