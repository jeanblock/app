import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { Title , Subheading, Paragraph, Button, IconButton, Colors} from 'react-native-paper';

export default function ItemProduct(props){

    let intrePice = props.price.toString();
    let princeArra = intrePice.split(".")
    let itenPrice = princeArra[0];
    function format( input ){
        var num = input.replace(/\./g,'');

        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
        num = num.split('').reverse().join('').replace(/^[\.]/,'');
        input = num;

        num = num.replaceAll(".", ",")
        return num
    }
    return(
        <View style={{ width: '100%', backgroundColor: "#fff" ,
            marginVertical: 10, padding: 16, borderRadius: 10, flexDirection: 'row',
            borderColor: '#EB0A1E'
        }}
        >
            <View style={{ width: '100%' , flexDirection: 'row', position: 'relative'}}>
                <Image source={{ uri: props.img }}
                    style={{ width: 80, height: 80, resizeMode: 'contain', borderRadius: 0 }}
                />
                <View style={{ marginLeft: 10 }}>
                    <Title style={{ width: "75%" , fontSize: 16, fontWeight: '600'}}>{props.name ? props.name : "Cargando..."}</Title>
                    {props.typeCover ? <Paragraph>Lonas: {props.typeCover}</ Paragraph> : null}
                    {props.lubType ? <Paragraph>Aplicación: {props.lubType}</ Paragraph> : null}
                    {props.presentation ? <Paragraph>Presentación: {props.presentation}</ Paragraph> : null}
                    {props.viscosity ? <Paragraph>Viscosidad: {props.viscosity}</ Paragraph> : null }
                    <Paragraph>Aplicación: {props.description}</ Paragraph>
                    <Paragraph>{props.brand}</ Paragraph>
                    <Paragraph style={{ fontWeight: '600', fontSize: 16 }}>Precio: { format(itenPrice) }.{princeArra[1]} ISV incluido</Paragraph>
                </View>
                <IconButton
                   icon={ props.selecItem == true ? require('../../assets/icons/comprobado.png')  : require('../../assets/icons/shopping-bag.png') }
                    color="#008cba"
                    size={25}
                    style={{ position: 'absolute', top: 15, right: 5, }}
                    onPress={() => props.event(props.data) }
                />
            </View>
        </View>
    )
}