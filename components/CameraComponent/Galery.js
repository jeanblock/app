import React, { useEffect, useState, createRef } from 'react';
import { View, Image, FlatList, PermissionsAndroid, Platform , TouchableOpacity, Text, SafeAreaView} from 'react-native';
import CameraRoll from "@react-native-community/cameraroll";
import { Appbar , Button , Snackbar, Colors} from 'react-native-paper';

export default function GaleryView(props){
    const [list, setLIst] = useState([]);
    const [select, setSelect] = useState("");
    const [visible, setVisible] = useState(false);

    useEffect( () => {
        async function getPermises(){
            if (Platform.OS === 'android') {
                const result = await PermissionsAndroid.request(
                  PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                  {
                    title: 'Acceso a tu Galeria',
                    message: 'Descuida toda la informacion es segura',
                  },
                );
                if (result !== 'granted') {
                  console.log('Access to pictures was denied');
                  return;
                }
              }
              CameraRoll.getPhotos({
                first: 500,
                assetType: 'Photos',
              })
              .then(r => {
                setLIst(r.edges);
              })
              .catch((err) => {
                  console.log(err)
                 //Error Loading Images
              });
        };
        getPermises();
    }, [] );

    function selectImage(e){
        setSelect(e)
        setVisible(true);
    };

    function selected(){
        console.log(select);
        props.save(select)
    };
    return(
        <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }} >
            <Appbar style={{ backgroundColor: Colors.blue900 }}>
                <Appbar.Action
                    icon={require('../../assets/icons/left-arrow.png')}
                    onPress={props.close }
                />
                <Appbar.Content title="Galeria"/>
            </Appbar>
            <FlatList
                data={list}
                numColumns={3}
                keyExtractor={ (item, index) => index.toString() }
                renderItem={({ item }) => 
                    <TouchableOpacity
                        onPress={ () => selectImage(item.node.image.uri) }
                        style={{
                            width: '31%',
                            height: 150,
                            margin: 4
                        }}
                    >
                    <Image style={{
                        width: '100%',
                        height: '100%',
                        borderRadius: 8,
                        borderWidth: select === item.node.image.uri ? 4 : 0,
                        borderColor: "red"
                        }}
                        source={{ uri: item.node.image.uri }}
                    />
                    </TouchableOpacity>
                }
            />
            <Snackbar
                visible={visible}
                onDismiss={()=> null}
                action={{
                    label: 'seleccionar',
                    onPress: selected,
                }}>
            Seleccionaste una imagen.
            </Snackbar>
        </SafeAreaView >
    )
}