import React, { useState , createRef, useRef} from 'react';
import { RNCamera } from 'react-native-camera';
import { StyleSheet, Text, Image, View , Alert , Modal } from 'react-native';
import { Button, IconButton} from 'react-native-paper';
import Galery from './Galery';

export default function CameraAPI(props){
    const [cameraType, setCameraType] = useState(RNCamera.Constants.Type.back);
    const [flash, setFlash] = useState({
        icon: "flash",
        type: RNCamera.Constants.FlashMode.on
    });
    const camera = createRef();
    const [img, setImg] = useState("");
    const [show, setShow] = useState(false);
    const [showGalery, setShowGalery] = useState(false);

    const takePicture = async () => {

        if (camera) {
          const options = { quality: 0.5, base64: true };
          const data = await camera.current.takePictureAsync(options);
          setShow(true);
          setImg(data.uri);
        }
    };
    function rotateCamera(){
        if(cameraType == RNCamera.Constants.Type.back){
            setCameraType( RNCamera.Constants.Type.front )
        }else{
            setCameraType( RNCamera.Constants.Type.back )
        }
    }
    function saveImage(){
        props.event(img, props.type);
        setShow(false);
    }
    function cancel(){
        setShow(false);
        setImg("");
    }
    function saveGalery(imgsEvent){
        props.event(imgsEvent, props.type);
        setShowGalery(false);
        setShow(false);
    }
    return (
        <View style={styles.container}>
            <RNCamera
                ref={camera}
                style={styles.preview}
                type={cameraType}
                flashMode={flash.type}
            />
            <View style={{
                flex: 0, flexDirection: 'row', justifyContent: 'space-between',
                position: 'absolute', bottom: 0, width: '100%', paddingHorizontal: 10,
            }}>
                <IconButton
                    mode="contain"
                    icon={ require('../../assets/icons/galeria-de-imagenes.png') }
                    size={35}
                    color={"#fff"}
                    onPress={() => setShowGalery(true)}
                />
                <IconButton
                    mode="contain"
                    icon={ require('../../assets/icons/diafragma.png') }
                    size={40}
                    color="#fff"
                    onPress={takePicture}
                />
                <IconButton
                    icon={ require('../../assets/icons/rotate-camera.png') }
                    size={35}
                    color={"#fff"}
                    onPress={rotateCamera}
                />
            </View>
            <Modal transparent={true} animationType="fade" visible={show}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' , padding: 16}}>
                    <View style={styles.modalBody}>
                        <View style={{ flex: 1 }}>
                            <Image source={{ uri: img }}
                                style={{ resizeMode: 'contain', height: 320, width: 300}}
                            />
                        </View>

                        <View style={styles.actions}>
                            <Button color="#EB4335" onPress={cancel}>Cancelar</Button>
                            <Button color="#EB4335" mode="contained" onPress={saveImage}>Guardar</Button>
                        </View>
                    </View>
                </View>
            </Modal>

            <Modal transparent={true} animationType="fade" visible={showGalery}>
                <Galery save={saveGalery} close={ () => setShowGalery(false) }/>
            </Modal>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      backgroundColor: '#fff',
      position: 'relative'
    },
    preview: {
      flex: 1,
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    capture: {
      flex: 0,
      backgroundColor: '#fff',
      borderRadius: 5,
      padding: 15,
      paddingHorizontal: 20,
      alignSelf: 'center',
      margin: 20,
    },
    modalBody:{
        width: "100%", height: '60%', borderRadius: 20, backgroundColor: "#fff",
        justifyContent: 'center', alignItems: 'center', padding: 16, flexDirection: 'column'
    },
    images:{
        width: '100%', height: 50, resizeMode: 'contain'
    },
    actions:{
        flexDirection: 'row', position: 'relative', bottom: 20, justifyContent: 'space-around', width: '100%', height: 40
    }
});