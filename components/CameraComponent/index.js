import React, {useState} from 'react';
import { View, Text, Modal , StyleSheet, TouchableOpacity} from 'react-native';
import CameraAPI from './CameraAPI';
import { IconButton, Colors } from 'react-native-paper';

export default function CameraComponent(event){
    const [show, setShow] = useState(false);
    const [type, setType] = useState("");
    const [imgs, setImgs] = useState([]);

    const hanleEvent = (e) => {
        setShow(true);
        setType(e);
    }
    const hableSaveFoto = (e, type) => {
        let imgLIst = imgs;
        let arrynew = imgLIst.filter( i => i.type != type  );
        
        arrynew.push({
            type: type,
            img: e
        });
        setShow(false);
        setImgs(arrynew);
        event.actions(arrynew);
    };

    return(
        <View>
            <Text style={Styles.title}>sube tus fotos</Text>
            <View style={Styles.listaFoto}>
                <TouchableOpacity style={Styles.catentICon} onPress={() => hanleEvent("one")}>
                    <IconButton
                        icon={ ( imgs.filter(i =>  i.type === "one" ).length ?  require('../../assets/icons/marca-de-verificacion.png') :  require('../../assets/icons/camera.png')  ) }
                        color={Colors.blue900}
                        size={40}
                        
                    />
                </TouchableOpacity>
                
                <TouchableOpacity style={Styles.catentICon}  onPress={() => hanleEvent("two")}>
                    <IconButton
                        icon={( imgs.filter(i =>  i.type === "two" ).length ?  require('../../assets/icons/marca-de-verificacion.png') :  require('../../assets/icons/camera.png')  )}
                        color={Colors.blue900}
                        size={40}
                    />
                </TouchableOpacity>

                <TouchableOpacity style={Styles.catentICon}  onPress={() => hanleEvent("three")} >
                    <IconButton
                        icon={( imgs.filter(i =>  i.type === "three" ).length ?  require('../../assets/icons/marca-de-verificacion.png') :  require('../../assets/icons/camera.png')  )}
                        color={Colors.blue900}
                        size={40}
                    />
                </TouchableOpacity>

            </View>
            <Modal transparent={false} animationType="slide" visible={show}>
                <View style={Styles.conten}>
                    <CameraAPI 
                        event={hableSaveFoto}
                        type={type}
                    />
                </View>
            </Modal>
        </View>
    );
}
const Styles = StyleSheet.create({
    conten: {
        flex: 1,
        backgroundColor: "#000"
    },
    title:{
        fontSize: 20,
       marginLeft: 16
    },
    catentICon:{
        marginTop: 10,
        marginLeft: 16,
        borderColor: Colors.blue900,
        borderWidth: 2,
        borderRadius: 10
    },
    listaFoto: {
        flexDirection: 'row'
    }
})