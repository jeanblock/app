import React from 'react';
import { View, Text , TouchableOpacity, Image, StyleSheet} from 'react-native';

export default function CarItem (props) {
   
    return (
        <TouchableOpacity style={[Styles.container, {
            backgroundColor: props.itemSelect ==  props.id ? "#2196f38c" : "#fff"
            }]} onPress={()=> props.selection ? props.selection(props.id, props.name) : null} >
            <View style={{ flex: 1.3 , padding:  8,  }}>
                <Image source={{ uri: props.img }}
                    style={Styles.image}
                />
            </View >
            <View style={{ flex: 2 }}>
                <Text style={{ fontSize: 20, marginBottom: 5, color: props.name === "Toyota" ? "#EB0A1E" :  props.name === "Ford" ? "#1351D8" : "#fafafa" }}>{props.name}</Text>
                <Text style={Styles.text}>VIN: {props.vin}</Text>
                <Text style={Styles.text}>Placa: {props.placa}</Text>
                <Text style={Styles.text}>Modelo: {props.model ? props.model :"cargando..." }</Text>
            </View>
        </TouchableOpacity>
    )
};

const Styles = StyleSheet.create({
    container: {
        flexDirection: 'row', marginBottom: 16,
        paddingVertical: 10,
        borderRadius: 15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 1,
    },
    image: {
        width: '100%', height: 80, 
        resizeMode: 'contain'
    },
    text: {
        fontSize: 16,
        marginBottom: 5
    }
})