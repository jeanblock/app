import React from 'react';
import { View, Text, TouchableOpacity , } from 'react-native';
import { IconButton, Colors, Badge  } from 'react-native-paper';
import { connect } from 'react-redux';
function StoreCar(props){
    return(
        <View style={{ flexDirection: 'row', position: 'relative' }}>
            <IconButton
                icon={ require('../../assets/icons/shopping-bag.png') }
                color="#008cba"
                size={25}
                onPress={() => props.show()}
                style={{ marginRight: 20 , marginTop: 10}}
            />
            <View style={{  position: 'absolute', right: 13 , top: 3}}>
                <Badge color="#eb0a1e">{props.cardStore.length}</Badge>
            </View>
        </View>
    )
};
const mapStateToProps = (state) => state;
export default connect(mapStateToProps, {})(StoreCar);