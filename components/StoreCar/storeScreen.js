import React, { useState } from 'react';
import { Text, View, FlatList, Image , Alert} from 'react-native';
import { List, IconButton , Button,  ActivityIndicator, Colors} from 'react-native-paper';
import { connect } from 'react-redux';
import {addAndDeleteProduct} from '../../Redux/Actions/cardStore.action';
import {generarCotizacion} from '../../apis/quotationAPI';
import CameraComponent from '../CameraComponent';
import storage from '@react-native-firebase/storage';

function CardScreen (props) {

    const [loading, setLoading] = useState(false);
    const [saveImgs, setSaveImgs] = useState([]);

    function deleteItem(id) {
        let card = props.cardStore;
        const listFilter = card.filter(i => i.id != id);
        props.addAndDeleteProduct(listFilter);
    }
    async function quotationGenerate() {
        setLoading(true);
        
        let productList = [];

        props.cardStore.forEach((i) => {
            productList.push(i.code)
        });

        let arrayImg = [];

        for await (let e of saveImgs) {
            const storageRef = storage().ref(`/profiles/${props.userData.user_sf_id}/${e.type}`);
            let files = await storageRef.putFile(e.img);

            if(files.state == "success"){
                let urlImga = await storage().ref(`/profiles/${props.userData.user_sf_id}/${e.type}`).getDownloadURL();
                arrayImg.push(urlImga);
            }
        }

        let objs = {
            "user_sf_id": props.userData.user_sf_id,
            "product_codes": productList,
            "imgs": arrayImg
        };

        
        generarCotizacion(objs).then(data => {
            if(data.code == 201){
                props.addAndDeleteProduct([]);
                Alert.alert(
                    'Hemos recibido tu solicitud',
                    'Uno de nuestros representantes te estará contactando a la brevedad posible.',
                    [
                      {
                        text: 'Aceptar',
                        onPress: () => console.log('Install Pressed')
                      }
                    ]
                );
                setLoading(false);
            }
        }).catch( () => {
            Alert.alert(
                'Ups!',
                'No hemos podido enviar tu cotización.',
                [
                  {
                    text: 'Aceptar',
                    onPress: () => console.log('Install Pressed')
                  }
                ]
            );
            setLoading(false);
        })
    }
    const hableListSaveImg = (event) => {
        setSaveImgs(event);
    }
    return (
        <View style={{ backgroundColor: "#fff", flex: 1 }} >
            <List.Section style={{ flex: 1 }} >
                { loading == false ? <>
                    <List.Subheader>Productos agregados a la bolsa:</List.Subheader>
                    <FlatList
                        data={props.cardStore}
                        keyExtractor={(item) => item.name }
                        renderItem={ ({item}) =>{
                            return (
                                <List.Item
                                    title={`${item.description} - ${item.brand}`}
                                    description={`Precio LPS ${item.price_isv} ISV incluido`}
                                    left={() =>
                                        <Image source={{ uri: item.picture_url }}
                                            style={{ width: 60, height: 60, resizeMode: 'cover'  }}
                                        />
                                    }
                                    right={() => < IconButton
                                            icon={require('../../assets/icons/basura.png')}
                                            color="red"
                                            onPress={ () => deleteItem(item.id) }
                                        />
                                    }
                                />
                            )
                        } }
                    />
                    
                    </> :
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <ActivityIndicator animating={true} color={Colors.red500} />
                        <Text style={{ textAlign: 'center', fontSize: 20, marginTop: 16 }} >Generando cotización...</Text>
                    </View>
                }
            </List.Section>
            {props.cardStore.length >=1 ?
            <>
                <CameraComponent
                    actions={hableListSaveImg}
                />
                <View style={{ flex: 0.2 }}>
                    <Button  mode="contained"
                        onPress={quotationGenerate}
                        style={{ margin: 16, backgroundColor: "#1B357E" }}
                    >
                        Cotizar
                    </Button>
                </View>
            </>

                : <View style={{ flex: 1 }}>
                    <Text style={{ textAlign: 'center', fontWeight: '700',
                        fontSize: 20
                    }}>
                        No hay productos en la bolsa
                    </Text>
                </View>

            }
        </View>
    )
 };
const mapStateToProps = (state) => state;
const mapDispachToProps = {
    addAndDeleteProduct
}
export default connect(mapStateToProps, mapDispachToProps )(CardScreen);