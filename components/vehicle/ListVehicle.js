import React, {useState} from 'react';
import { View, Text, FlatList } from 'react-native';
import CarItem from '../cars/cardItem';
import { connect } from 'react-redux';

function ListVehicle (props) {
    const [select, setSelect] = useState("");
    function selection(e, name){
        props.select(e, name);
        setSelect(e);
    }
    
    return (
      <View style={{ padding: 14, backgroundColor: '#fff' }} >
        <Text style={{ fontSize: 20, fontWeight: '700', marginBottom: 15 }}> Selecciona un vehiculo </Text>
        <FlatList
            data={props.vehicleData}
            keyExtractor={ (item) => item.id.toString() }
            style={{ padding:  3 }}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            renderItem={ ({item}) => {
                return <CarItem
                        img={item.image_url}
                        name={item.brand}
                        placa={item.plate}
                        vin={item.VIN}
                        id={item.id}
                        years={item.year}
                        selection={selection}
                        itemSelect={select}
                        model={item.model}
                    />
                }
            }
        />
      </View>
    );

}
const mapStateToProps = (state) => state
export default connect(mapStateToProps, {})(ListVehicle) ;
