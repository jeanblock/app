import React, {useState, useEffect} from 'react';
import { View, Text , TouchableOpacity} from 'react-native';
import { Avatar, Button } from 'react-native-paper';
import { connect } from 'react-redux';
import {dataUserActions} from '../../Redux/Actions/userData.action';
function ProfileComponents(props){
    function update(){
        props.dataUserActions();
    }

    useEffect(()=> {
        update();
    }, []);

    return(
        <View style={{ padding: 16, flexDirection: 'row', backgroundColor: "#fff" }}>
            <Avatar.Image size={80} source={{ uri: props.userData.photo_url }} />
            <View style={{ marginLeft: 20, paddingVertical: 10 }}>
                <Text style={{ fontSize: 20, fontWeight: '600' }}>
                    {props.userData.first_name ? props.userData.first_name: "Cargando" } {props.userData.last_name ? props.userData.last_name : "..."}
                </Text>
                < TouchableOpacity
                    style={{ width: 130 , marginTop: 5}}
                    onPress={()=> props.navigate.navigate("Profile")}
                >
                    <Text style={{ fontSize: 16 , color: "#008cba"}}>
                        Ver perfil
                    </Text>
                </ TouchableOpacity>
            </View>
        </View>
    )
};
const mapStateToProps = (state) => state;
const mapDispachToProps = {
    dataUserActions
}
export default connect(mapStateToProps, mapDispachToProps)(ProfileComponents);